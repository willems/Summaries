# Fine-tuning

With just a few hundred examples tune a model to specific task, which is truly amazing.

## Instruction fine-tuning

### fine tune an LLM with instruction prompts
Limitations of In-context learning (ICL)

- for smaller models, it doesn't always work
- any examples you include in your prompt take up valuable space in the context window, reducing the amount of room you have to include other useful information

Solution: fine tuning a base model

![Fine Tune abstract](images/FineTune1.png)

One strategy **instruction fine tuning**, is particularly good at improving a model's performance on a variety of tasks: trains the model using examples that demonstrate how it should respond to a specific instruction.

The data set you use for training includes many pairs of prompt completion examples for the task you're interested in, each of which includes an instruction.

![Fine Tune with instructions](images/FineTune2.png)

**Instruction fine-tuning**, where all of the model's weights are updated is known as **full fine-tuning**.
Outcome is a new version of the model, with new weights. Requieres enough budget and compute power.

First collect training data and transform in instruction prompt datasets, using templates.

![Sample prompt instruction templates](images/SampleTemplatesPromtInstructions.png)

Then

![Sample prompt instructions Split](images/SampleTemplatesPromtInstructions2.png)

Feed into the LLM and compare output with input (training data) Output is a probability distibution. So compare also the other results. Calculate loss with Cross-Entropy. The ipdate the weights of the LLM with standard progagation. Several epoch, so model performance improves.
This wil give te validation-accuracy and test-accuracy.
The new model is called an **instruct model**.

## Fine-tuning on a single task

Good results can be achieved with **relatively few examples**. Often just 500-1,000 examples can result in good performance in contrast to the billions of pieces of texts that the model saw during pre-training.

Downside: The process may lead to a phenomenon called **catastrophic forgetting**.
Catastrophic forgetting happens because the full fine-tuning process modifies the weights of the original LLM. While this leads to great performance on the single fine-tuning task, it can degrade performance on other tasks.

Options to avoid catastrophic forgetting:

- Does catastrophic forgetting actually impacts your use case? Good multitask fine-tuning may require 50-100,000 examples across many tasks, and so will require more data and compute to train.
- perform parameter efficient fine-tuning (PEFT). PEFT hows greater robustness to catastrophic forgetting since most of the pre-trained weights are left unchanged.

## Multi-task instruction fine-tuning

Training dataset is comprised of example inputs and outputs for multiple tasks. Improve the performance of the model on all the tasks simultaneously, thus avoiding the issue of catastrophic forgetting.

![Multi Task instruction Fine-Tuning](images/MultiTaskInstruction1.png)

One drawback to multitask fine-tuning is that it requires a lot of data. You may need as many as 50-100,000 examples in your training set.

Example:
FLAN family of models. FLAN, which stands for fine-tuned language net, is a specific set of instructions used to fine-tune different models.

![Multi Task instruction Fine-Tuning](images/MultiTaskInstruction2.png)

Those datasets are chosen from other models and papers as shown here

![Multi Task instruction Fine-Tuning](images/MultiTaskInstruction3.png)

In practice, you'll get the most out of fine-tuning by using your company's own internal data.

[Scaling Instruction-Finetuned Language Models](images/ScalingInstruction-FinetunedLanguageModels.pdf)

## Model evaluation

LLMs the output is non-deterministic in comparison with ML models ie accuracy is used (Correct Prediction/Total Predictions)

**ROUGE** and **BLEU** simple evaluation metrics:

- ROUGE (recall oriented under study for jesting evaluation) is primarily employed to assess the quality of automatically generated summaries by comparing them to human-generated reference summaries.
- BLEU (bilingual evaluation understudy) is an algorithm designed to evaluate the quality of machine-translated text, by comparing it to human-generated translations.

![LLM Evaluation ROUGE BLUE](images/LLMEnaluation1.png)

![LLM Evaluation ROUGE BLUE](images/LLMmetricsTerminology.png)

![ROUGE-1](images/ROUGE1.png)

![ROUGE-2](images/ROUGE2.png)

![ROUGE-L](images/ROUGEL.png)

![ROUGE-clipping](images/ROUGEclipping.png)

## Benchmarks

In order to measure and compare LLMs more **holistically**, you can make use of pre-existing datasets, and associated benchmarks that have been established by LLM researchers specifically for this purpose.**Selecting the right evaluation dataset is vital**, so that you can accurately assess an LLM's performance, and understand its true capabilities.

An important issue that you should consider is whether the model has seen your evaluation data during training

Evaluation Benchmarks

- GLUE (is a collection of natural language tasks, such as sentiment analysis and question-answering; 2018)
- SuperGLUE (As a successor to GLUE, 2019, to address limitations in its predecessor. SuperGLUE includes tasks such as multi-sentence reasoning, and reading comprehension.) [SuperGLUE](https://super.gluebenchmark.com/) and [SuperGLUE Leaderboard](https://gluebenchmark.com/leaderboard/)
- MMLU (massive Multitask Language Understanding)
- BIG-bench

![Benchmaks for Massive Models](images/BenchmarksMassiveModels.png)

- HELM (Holistic Evaluation of Language Models)

![HELM](images/HELM.png)

## Parameter efficient fine-tuning (PEFT)

Full fine-tuning where every model weight is updated during supervised learning, parameter efficient fine tuning methods **only update a small subset of parameters**.

- techniques freeze most of the model weights and focus on fine tuning a subset of existing model parameters
- don't touch the original model weights at all, and instead add a small number of new parameters or layers and fine-tune only the new components.
- With **PEFT**, most if not all of the LLM weights are kept frozen. As a result, the number of trained parameters is much smaller than the number of parameters in the original LLM. PEFT can often be performed on a **single GPU**.PEFT is **less prone to the catastrophic forgetting problems** of full fine-tuning.

![PEFT](images/PEFT1.png)

The PEFT weights are trained for each task and can be easily swapped out for inference, allowing efficient adaptation of the original model to multiple tasks.

![PEFT](images/PEFT2.png)

Several methods each with trade-offs on parameter efficiency, memory efficiency, training speed, model quality, and inference costs.

![PEFT](images/PEFT3.png)

- Adapter methods add new trainable layers to the architecture of the model,typically inside the encoder or decoder components after the attention or feed-forward layers.
- Soft prompt methods keep the model architecture fixed and frozen, and focus on manipulating the input to achieve better performance. This can be done by adding trainable parameters to the prompt embeddings or keeping the input fixed and retraining the embedding weights

## LoRA (LOW-RANK ADAPTATION OF LARGE LANGUAGE MODELS)

[LoRA paper](images/LoRA.pdf)

[QLoRA paper](images/QLora.pdf)

Re-parameterization

![LoRA](images/LoRA1.png)

![LoRA Example](images/LoRA2.png)

Use LoRA to train many tasks

![LoRA many tasks](images/LoRA3.png)

In principle, the smaller the rank r, the smaller the number of trainable  parameters, and the bigger the savings on compute. However, there are some issues related to model performance to consider.
LoRA is broadly used in practice because of the comparable performance to full fine tuning for many tasks and data sets

**QLoRA**: combine LoRA with the quantization techniques; further reduce memory footprint.


## Soft prompts


[The Power of Scale for Parameter-Efficient Prompt Tuning paper](images/PromptTuning.pdf)

prompt tuning is NOT prompt engineering

![Soft Prompt](images/Softprompt1.png)

Only the soft prompt weights are updated.

![Soft Prompt](images/Softprompt2.png)

Prompt tuning can be as effective as full fine tuning and offers a significant boost in performance over prompt engineering alone, for models with large numbers of parameters.
The words closest to the soft prompt tokens have similar meanings. The words identified usually have some meaning related to the task, suggesting that the prompts are learning word like representations.


Python libs
evaluate
rouge_score
loralib
peft
