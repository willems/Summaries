# MLOps course DeepLearning.ai

> MLOps is an ML engineering culture and practice that aims at unifying ML development (Dev) and ML operation (Ops)” [^1]

## Automation and Monitoring

at all steps of ML system construction, including:

- integration
- testing
- releasing
- deployment
- infrastructure management

## The MLOps framework

![from tensorflow tfx](https://www.tensorflow.org/static/tfx/guide/images/libraries_components.png)

In this course only the following steps are explained:

- Data Transformation
- Model
- Serving

Job Orchestration:
 you telling the system which steps to execute first and the following steps in the right sequence.

One model per use case (so one dimentional)

## MLOps vs System Design for LLMs

|MLOps for LLMs (LLMOps)|LLM System design|
|----|---|
|Focus on the LLM development and managing the model in production|Broader design of the entire end-to-end application (front-end back-end, data engineering etc.)|
|- experiment on foundation models|- chain multiple LLM together|
|- prompt design and management|- grounding|
|- supervised tuning|- track history|
|- monitoring||
|- evaluation generative output||

### What is Grounding?

Grounding is the process of using large language models (LLMs) with information that is use-case specific, relevant, and not available as part of the LLM's trained knowledge. [^2]

![example](images/examplePossibleWorkflow.png)
(green boxes are discussed in this course)

![LLMOps Pipeline](images/LLMOpsPipelines.png)

### LLMOps topics beyond the scope of this course

- Prompt design and prompt management
- Model evaluation
- Model monitoring
- Testing

### File formats train and evaludation

- JSONL: JSON Lines (JSONL) is a simple text-based format with each question and answer on a row. It is human readable and an ideal choice for small to medium-sized datasets.
- TFRecord: Binary format and easier to read for
computers, making it ideal for efficient training.
- Parquet: Parquet is a good choice for large and complex
datasets.

### Versioning artifacts

- Keep track of artifacts
- versioning of data: from which data you generated the datafile => tracability
- reproducecability, maintainability

## MLOps workflows for LLMs

Orchestration: which step first, the next step etc
Automation: the flow is automated
Deployment: trake trained model and put in production
Tools for orchestration and automation: Airflow or Kubeflow [Kube for pipelines](https://www.kubeflow.org/docs/components/pipelines/v2/)

![workflow LLMs](images/WorkflowLLMs.png)

**Reuse of pipelines is important!**

## Beyond deployment

- Package, Deploy and version.
- Model monitoring: metrics and safety.
- Inference scalability:
  - Load test
  - controlled roll out, etc.
- Latency: permissible latency.
  - Smaller models
  - Faster processors (GPUs, TPUs)
  - Deploy regionally

[^1]: <https://cloud.google.com/blog/products/ai-machine-learning/key-requirements-for-an-mlops-foundation>

[^2]: <https://techcommunity.microsoft.com/t5/fasttrack-for-azure/grounding-llms/ba-p/3843857>
