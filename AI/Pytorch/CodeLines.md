# Pytorch

## Run a docker container

This container runs in background

```bash
docker run -d -it --gpus all -name pytorch-container pytorch/pytorch:latest
```

## Jupyter container with pytorch for GPU
```bash
docker run -e JUPYTER_ENABLE_LAB=yes -v /home/john/Work/pytorch/:/workspace/dev --gpus all --ipc=host --ulimit memlock=-1 --ulimit stack=67108864 --rm -p 8888:8888 jw/pytorch:0.1
```

## Test GPU
```python
import torch
print(torch.cuda.is_available())
print(torch.cuda.device_count())
print(torch.cudaget_device_name(1))

import psutil
print(psutil.virtual_memory())  ## in bytes
```


## Connect to running container

```bash
docker exec -it <container-name> bash
docker exec -it <container-name> bash -c "cat aap"
```

## Stop and start an execistion container

```bash
docker stop <container-name>
docker start <container-name>
```

## Example code

```python
# Load libraries
import torch
import torch.nn as nn
from res.plot_lib import set_default, show_scatterplot, plot_bases
from matplotlib.pyplot import plot, title, axis
```

```python
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
```
