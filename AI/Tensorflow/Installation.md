
# Installation Tensorflow

## Installation (no need anymore, see conda install)

[Installation cuda](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#ubuntu-installation)
[Download page](https://developer.nvidia.com/cuda-downloads)

### Installation (possible different version)

wget <https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin>
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget <https://developer.download.nvidia.com/compute/cuda/11.5.0/local_installers/cuda-repo-ubuntu2004-11-5-local_11.5.0-495.29.05-1_amd64.deb>
sudo dpkg -i cuda-repo-ubuntu2004-11-5-local_11.5.0-495.29.05-1_amd64.deb
sudo apt-key add /var/cuda-repo-ubuntu2004-11-5-local/7fa2af80.pub
sudo apt-get update
sudo apt-get -y install cuda

sudo apt-get install cuda-compat-11-5

[Install cuddn](https://docs.nvidia.com/deeplearning/cudnn/archives/cudnn-824/install-guide/index.html#install-linux)
[Download page](https://developer.nvidia.com/rdp/cudnn-download)

## Unzip the cuDNN package

tar -xzvf cudnn-x.x-linux-x64-v8.x.x.x.tgz

## Copy the following files into the CUDA Toolkit directory

$ sudo cp cuda/include/cudnn*.h /usr/local/cuda/include
$ sudo cp -P cuda/lib64/libcudnn*/usr/local/cuda/lib64
$ sudo chmod a+r /usr/local/cuda/include/cudnn*.h /usr/local/cuda/lib64/libcudnn*

[Post actions](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#post-installation-actions)

## environment variables add to .bashrc

export LD_LIBRARY_PATH=/usr/local/cuda-11.5/lib64\${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

export PATH=/usr/local/cuda-11.5/bin${PATH:+:${PATH}}
