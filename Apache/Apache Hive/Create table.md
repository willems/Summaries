---
title: Create table
updated: 2022-04-04 11:59:19Z
created: 2022-04-03 12:44:49Z
---

```hive
CREATE [TEMPORARY] TABLE employee (
name STRING,
work_place ARRAY<STRING>,
gender_age STRUCT<gender:STRING,age:INT>,
skills_score MAP<STRING,INT>,
depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
COLLECTION ITEMS TERMINATED BY ','
MAP KEYS TERMINATED BY ':'
STORED AS TEXTFILE;

LOAD DATA INPATH '/user/aap/data/employee.txt'
OVERWRITE INTO TABLE employee;
```

```hive
CREATE TABLE IF NOT EXISTS employee_hr (
name string,
employee_id int,
sin_number string,
start_date date
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|';

LOAD DATA INPATH '/user/aap/data/employee_hr.txt'
OVERWRITE INTO TABLE employee_hr
```

```hive
CREATE TABLE employee_id (
name STRING,
employee_id INT,
work_place ARRAY<STRING>,
gender_age STRUCT<gender:STRING,age:INT>,
skills_score MAP<STRING,INT>,
depart_title MAP<STRING,ARRAY<STRING>>
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
COLLECTION ITEMS TERMINATED BY ','
MAP KEYS TERMINATED BY ':';

LOAD DATA INPATH
'/user/aap/data/employee_id.txt'
OVERWRITE INTO TABLE employee_id
```

```hive
CREATE TABLE IF NOT EXISTS employee_contract (
name string,
dept_num int,
employee_id int,
salary int,
type string,
start_date date
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
STORED as TEXTFILE;

LOAD DATA INPATH '/user/aap/data/employee_contract.txt'
OVERWRITE INTO TABLE employee_contract;
```

```hive
CREATE TABLE ctas_employee as SELECT * FROM employee
```

```hive
CREATE VIEW IF NOT EXISTS employee_skills
AS
SELECT
name, skills_score['DB'] as DB,
skills_score['Perl'] as Perl,
skills_score['Python'] as Python,
skills_score['Sales'] as Sales,
skills_score['HR'] as HR
FROM employee;
```
