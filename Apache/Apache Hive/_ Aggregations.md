---
title: '# Aggregations'
updated: 2022-04-03 17:09:07Z
created: 2022-04-03 17:00:47Z
---

```hive
SELECT 
sum(CASE WHEN gender_age.gender = 'Male' THEN gender_age.age ELSE 0 END)/
count(CASE WHEN gender_age.gender = 'Male' THEN 1
ELSE NULL END) as male_age_avg
FROM employee;

SELECT
sum(coalesce(gender_age.age,0)) as age_sum,
sum(if(gender_age.gender = 'Female',gender_age.age,0)) as female_age_sum
FROM employee;

SELECT
if(name = 'Will', 1, 0) as name_group,
count(name) as name_cnt
FROM employee
GROUP BY if(name = 'Will', 1, 0);
```

```hive
SELECT
count(DISTINCT gender_age.gender) as gender_uni_cnt,
count(DISTINCT name) as name_uni_cnt
FROM employee;
```
