---
title: '# DDL'
updated: 2022-04-04 20:03:49Z
created: 2022-04-03 13:46:28Z
---

```hive
SHOW CREATE TABLE employee

SHOW TABLES
SHOW TABLES '*em*'
SHOW VIEWS

SHOW COLUMNS IN employee
DESC employee  | DESCRIBE employee
SHOW TBLPROPERTIES employee
```
