---
title: '# Functions'
updated: 2022-04-03 16:58:17Z
created: 2022-04-03 13:36:09Z
---

```hive
SELECT concat('1','+','3','=',cast((1 + 3) as string)) as res;

SELECT
SIZE(work_place) as array_size,
SIZE(skills_score) as map_size,
SIZE(depart_title) as complex_size,
SIZE(depart_title["Product"]) as nest_size
FROM employee;

SELECT
array_contains(work_place, 'Toronto') as is_Toronto,
sort_array(work_place) as sorted_array
FROM employee;
```

## Date

```hive
SELECT TO_DATE(FROM_UNIXTIME(UNIX_TIMESTAMP())) as currentdate;
```

```hive
SELECT
reverse(split(reverse('/user/john/data/employee.txt'),'/')[0])
as linux_file_name;
```

## Opposite of explode

```hive
SELECT
collect_set(gender_age.gender) as gender_set,
collect_list(gender_age.gender) as gender_list
FROM employee;
```

