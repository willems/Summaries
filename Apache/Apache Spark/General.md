---
title: General
updated: 2022-05-24 19:25:58Z
created: 2022-05-24 19:20:33Z
---

What is Apache Spark™?
Apache Spark™ is a multi-language engine for executing data engineering, data science, and machine learning on single-node machines or clusters.

### Batch/streaming data
> Unify the processing of your data in batches and real-time streaming, using your preferred language: Python, SQL, Scala, Java or R.

### SQL analytics
> Execute fast, distributed ANSI SQL queries for dashboarding and ad-hoc reporting. Runs faster than most data warehouses. 

### Data science at scale
> Perform Exploratory Data Analysis (EDA) on petabyte-scale data without having to resort to downsampling 

### Machine learning
> Train machine learning algorithms on a laptop and use the same code to scale to fault-tolerant clusters of thousands of machines. 

[Difference between Spark DataFrame and Pandas DataFrame](https://www.geeksforgeeks.org/difference-between-spark-dataframe-and-pandas-dataframe/)