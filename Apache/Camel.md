---
title: Camel
updated: 2022-05-24 19:42:30Z
created: 2022-05-24 19:38:56Z
---

Apache Camel is an Open Source integration framework that empowers you to quickly and easily integrate various systems consuming or producing data. 

Apache Camel™ is a versatile open-source integration framework based on known Enterprise Integration Patterns.

Camel empowers you to define routing and mediation rules in a variety of domain-specific languages (DSL, such as Java, XML, Groovy, Kotlin, and YAML). This means you get smart completion of routing rules in your IDE, whether in a Java or XML editor.

[source](https://camel.apache.org/)