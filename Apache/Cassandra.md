---
title: Cassandra
updated: 2022-05-24 19:28:24Z
created: 2022-05-24 19:26:51Z
---

What is Apache Cassandra?
> Apache Cassandra is an open source NoSQL distributed database trusted by thousands of companies for scalability and high availability without compromising performance. Linear scalability and proven fault-tolerance on commodity hardware or cloud infrastructure make it the perfect platform for mission-critical data.

###  Distributed
> Cassandra is suitable for applications that can’t afford to lose data, even when an entire data center goes down. There are no single points of failure. There are no network bottlenecks. Every node in the cluster is identical.

