---
title: Fluentd
updated: 2022-05-24 19:54:57Z
created: 2022-05-24 19:53:01Z
---

Fluentd is an open source data collector, which lets you unify the data collection and consumption for a better use and understanding of data.

![c2a9b03791fddf1aabea180f18076f55.png](../_resources/c2a9b03791fddf1aabea180f18076f55.png)