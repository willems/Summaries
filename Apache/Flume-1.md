---
title: Flume
updated: 2022-05-24 19:52:11Z
created: 2022-05-24 19:50:29Z
---

Flume is a distributed, reliable, and available service for efficiently collecting, aggregating, and moving large amounts of log data. It has a simple and flexible architecture based on **streaming data** flows. It is robust and fault tolerant with tunable reliability mechanisms and many failover and recovery mechanisms. It uses a simple extensible data model that allows for online analytic application.

![e3bb1a88acbda798341b1985f38d888c.png](../_resources/e3bb1a88acbda798341b1985f38d888c.png)

