---
title: RabbitMQ
updated: 2022-05-24 19:49:57Z
created: 2022-05-24 19:45:41Z
---

RabbitMQ is the most widely deployed open source message broker.

### Asynchronous Messaging
> Supports multiple messaging protocols, message queuing, delivery acknowledgement, flexible routing to queues, multiple exchange type.


[source](https://www.rabbitmq.com)
