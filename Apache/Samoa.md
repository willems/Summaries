---
title: Samoa
updated: 2022-05-24 18:34:24Z
created: 2022-05-24 18:30:37Z
---

# Scalable Advanced Massive Online Analysis

Apache SAMOA is a platform for mining on big data streams. It is a distributed streaming machine learning (ML) framework that contains a programming abstraction for distributed streaming ML algorithms.

Apache SAMOA enables development of new ML algorithms without dealing with the complexity of underlying streaming processing engines (SPE, such as Apache Storm and Apache S4). Apache SAMOA also provides extensibility in integrating new SPEs into the framework. These features allow Apache SAMOA users to develop distributed streaming ML algorithms once and to execute the algorithms in multiple SPEs, i.e., code the algorithms once and execute them in multiple SPEs.

[samoa-project](https://samoa-project.net/)