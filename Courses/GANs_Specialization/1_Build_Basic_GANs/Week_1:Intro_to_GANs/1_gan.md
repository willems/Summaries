## Generative Models

![Generative vs Discriminative models](Images/Generative_vs_Discriminative_models.png)

- Discriminative models -> classifiers
- The noise is larger to ensure that what's generated isn't actually the same dog each time
- if you're only generating one class, one Y of a dog, then you probably don't need this conditioning on Y and instead it's just the probability over all the features X

![two of the most popular generative model architectures](Images/Generative_Models.png)

The two models in a GAN compete with each other and reach a point where realistic examples are produced by the generator.

In the end, the Generator is not needed anymore, and only some random noise is used as input to generate some realistic image.

## Goal of the generator and the discriminator

Generator learns to make **fake** that looks **real** and tries to fool the discriminator.
Discriminator learns to **distinguish real** from **fake**, by looking to real and fake.

## Competition between generator and the disciminator

In the beginning the generator is not very sophisticated.
The generator is not to see the real.

The discriminator start to learn the real.
Then the discriminator start to decide what is real and what is fake.

When the generator starts to produce bathes of fake, the discriminator will know in what direction to go and improve to decide fake or real.

End: fakes look real.

## Discriminator

- is a classifier

- not only images, but also eq text

- P(Y|X)  => P(Class | Features)
    conditional probability distribution

- CLass is fake: so the discriminator will determinate a probalility how fake a class is.
  ![Disriminator probablility](Images/Dicriminator_probability.png)

- this probability is given back to generator.

## Generator

Goal: produce example of a certain class
Noise vector: result is different outputs at every run

![Generator Learning](Images/Generator_learning.png)

When good enough save the value θ (theta)

![Saved Theta](Images/Saved_theta.png)

If one class then for Generator P(X)
if multiple classes then P(X|Y)

## BCE Cost Function

Binary Cross Entropy function, or BCE for short, is used for training GANs.

Desinged for classification when there are two categories: real and fake.

![BCE_Function](Images/BCE_function.png)

Negative sign: ensures that the cost is always>=0

![BCE_LOss_Function](Images/BCE_Loss_function.png)

Summary:

- BCE Cost function has two parts
- Close to zero when the label and prediction are similar
- Appraches infinity when the label and prediction are different

## Put it all together

![BCE_LOss_Function](Images/GANs_Model.png)

![BCE_LOss_Function](Images/Training_GAN_discriminator.png)
Only the parameters of the Discriminator are updated!!

The discriminator looks at real and fake images over time, makes guesses, and gets feedback on whether its guess was right or wrong.

Over time, it learns to discern real from fake better, but note that since the generator is also learning, the fake images get more realistic and harder to discern. This cat and mouse game enables both models to learn in tandem. 

![BCE_LOss_Function](Images/Training_GANs_Generator.png)

With feedback from the discriminator on whether a fake image looks real or fake, the generator starts producing fake images that are more and more realistic (that tries to fool the discriminator). Images that look “fake” to the discriminator are discarded in favor of those that look “real” to the discriminator; since the discriminator is improving over time as it sees more reals and fakes, the generator needs to continually learn too.

So as you alternate their training, only **one model is trained at a time**, while the other one is held constant. So in training GANs in this alternating fashion, it's important to keep in mind that **both models should improve together and should be kept at similar skill levels** from the beginning of training. 

![BCE_LOss_Function](Images/Traing_gan_similar_level.png)

One common issue is having a **superior discriminator**, having this discriminator learn too quickly. And when it learns too quickly and it suddenly looks at a fake image and says, this is 100% fake. This 100% is not useful for **the generator at all because it doesn't know which way to grow and learn**

![BCE_LOss_Function](Images/Pytorch_vs_Tensorflow.png)