# Wasserstein GANs with Gradient Penalty

Goals:

- Examine the cause and effect of an issue in GAN training known as mode collapse.
- Implement a Wasserstein GAN with Gradient Penalty to remedy mode collapse.
- Understand the motivation and condition needed for Wasserstein-Loss.

## Mode Collapse

Mode is distributions
Mode collapse in GANs
Intuition behind it during training

A mode in a distribution of data is just an area with a high concentration of observations.

Modes are peaks in the distribution of features amd are typical with real-world datasets.
![Modes](./Images/ModeCollapse.png)

Collapsing to one mode or fewer modes and some of the modes disappearing.

**Mode collapse occurs when the generator gets stuck generating one mode.** (Generator is at a local minimum) The discriminator will eventually learn to differentiate the generator's fakes when this happens and outskill it, ending the model's learning.

## Problem with BCE Loss

With BCE loss GANs are prone to mode collapse and other problems.

GANs trained with BCE loss are susceptible to vanishing gradient problems

The higher this cost value (of BCE) is, the worse the discriminator is doing at it.
![BCE in GANs](./Images/BCEInGANs.png)
Often called a **minimax game**

 GANs try to make the generated distribution look similar to the real one by minimizing the underlying cost function that measures how different the distributions are.

During training it's possible for the discriminator to outperform the generator, very possible, in fact, quite common. But at the beginning of training, this isn't such a big problem because the discriminator isn't that good. It has trouble distinguishing the generated and real distributions. There's some overlap and it's not quite sure. As a result, it's able to give useful feedback in the form of a non-zero gradient back to the generator. However, as it gets better at training, it starts to delineate (afbakenen) the generated and real distributions a little bit more such that it can start distinguishing them much more. Where the real distribution will be centered around one and the generated distribution will start to approach zero. As a result, when it's starting to get better, as this discriminator is getting better, it'll start giving less informative feedback. In fact, it might give gradients closer to zero, and that becomes unhelpful for the generator because then the generator doesn't know how to improve. This is how the vanishing gradient problem will arise.

What is the problem with using BCE Loss?
The discriminator does not output useful gradients (feedback) for the generator when the real/fake distributions are far apart.

## Earth Mover’s Distance (EMD)

Is a cost function => solves vanishing gradient problem of BCE.

What does Earth Mover’s distance measure?
Earth mover’s distance is a measure of how different two distributions are by estimating the effort it takes to make the generated distribution equal to the real one.

With Earth mover's distance there's no such ceiling to the zero and one. So the cost function continues to grow regardless of how far apart these distributions are. The gradient of this measure won't approach zero and as a result, GANs are less prone to vanishing gradient problems
![EDM](./Images/EDM.png)

Earth mover's distance is a function of the effort to make a distribution equal to another. So it depends on both distance and amount. Reduces the likelihood of mode collapse in GANs

## Wasserstein Loss (W-loss)

approximates the Earth Mover's Distance.

![W-loss vs BCE](./Images/W-lossvsBCE.png)

Some of the main differences between W-Loss and BCE Loss is that, the discriminator under BCE Loss outputs a value between 0 and 1, while the critic in W-Loss will output any number.

W-loss doesn't have a vanishing gradient problem, and this will mitigate against mode collapse, because the generator will always get useful feedback back.

Discriminator => Critic (different name because it is no classifier anymore)

## Condition on Wasserstein Critic

There is a special condition that needs to be met by the critic.

W-Loss is a simple expression that computes the difference between the expected values of the critics output for the real examples x and its predictions on the fake examples g(z). The generator tries to minimize this expression, trying to get the generative examples to be as close as possible to the real examples while the critic wants to maximize this expression because it wants to differentiate between the reals and the fakes, it wants the distance to be as large as possible.

- Critics -> max
- Generator -> min

![Condition W-Loss](./Images/Condition-W-Loss.png)

For a function like the critics neural network to be at 1-Lipschitz Continuous, the norm of its gradient needs to be at most one. What that means is that, the slope can't be greater than one at any point, its gradient can't be greater than one.

![1 Lipschitz](./Images/1-Lipschitz.png)

The slope can not be greater than 1 at any point on a function in order for it to be 1-Lipschitz Continuous.

This condition on the critics neural network is important for W-Loss because it assures that the W-Loss function is not only continuous and differentiable, but also that it doesn't grow too much and maintain some stability during training.

## 1-Lipschitz Continuity Enforcement

One Lipschitz continuity or 1-L continuity of the critic neural network in your Wasserstein loss and gain ensures that Wasserstein loss is valid.

How enforce:

- **weight clipping**: the weights of the critics neural network are forced to take values between a fixed interval.
 After you update the weights during gradient descent, you actually will clip any weights outside of the desired interval. Basically what that means is that weights over that interval, either too high or too low, will be set to the maximum or the minimum amount allowed.
There's a lot of hyperparameter tuning involved.
- **gradient penalty** (works better)
all you need to do is add a regularization term to your loss function. What this regularization term does to your W loss function, is that it penalizes the critic when it's gradient norm is higher than one.
![Gradient Penalty](./Images/Gradient-penalty.png)

Gradient penalty during implementation, of course, all you do is sample some points by interpolating between real and fake examples. Sample all points is not practical.

![Gradient Penalty](./Images/Gradient_Penalty_1.png)

It's on X hat that you want to get the critics gradient to be less than or equal to one.

![Put it all together](./Images/Recap.png)
