#!/usr/bin/env bash

touch one.txt \
	&& touch two.txt \
	&& touch three.txt

	ls *.txt | wc -l

rm one.txt two.txt three.txt
