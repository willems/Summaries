find . -name "*.txt"

find executable files, non-invisible
find . -perm /+x ! -name '.*' -type f


Find all executable non-invisible files and non-invisible directories
find . -perm /+x -not -path '*/\.*' -type f
find . -perm /+x -not -path '*/\.*' -name "*.sh" -type f

# Find logs over 10 MB, tar them up
find /var/log -type f -size +10M -print | xargs tar -cvzf logs.tar.gz

