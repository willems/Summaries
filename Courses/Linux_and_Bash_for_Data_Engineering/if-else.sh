#!/bin/sh
#
echo "What food do you like?"
read FOOD

if [ "$FOOD" = "Apple" ]; then
	echo "Eat yogurt with your ${FOOD}"
elif [ "$FOOD" = "Milk" ]; then
	echo "Eat Cereal with your ${FOOD}"
else
	echo "Eat your ${FOOD} by itself" 
fi
