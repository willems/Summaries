#!/usr/bin/bash -xv
#
set -e
set -x
set -v
declare -a array=("apple" "pear" "cherry")

for i in "${array[@]}"
do
	echo "this ${i} is delicious!"
done
