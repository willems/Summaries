#!/usr/bin/env bash

# Counts - Number of times to print phrase
count=5

# Phrase - Message to print 
phrase="hello world"

# Reverse - Whether to reverse string
concat=0



# Parse options
while [[ $# -gt 0 ]]; do
   key="$1"
   case $key in
     -c|--count)
        count="$2"
        shift   
        ;;
     -p|--phrase)
        phrase="$2"
        shift
        ;;
     -r|--reverse)
        phrase=$(echo $phrase| rev)
        ;;
     --concatenate)
        concat=1
        ;;
     --capitalize)
        phrase=$(echo $phrase | tr '[:lower:]' '[:upper:]')
        ;;
     -d|--delimiter)
        delim="$2"
        shift
        ;;
    esac
    shift
done

concatenate() {
  result=$phrase
  for ((i=0; i<$count; i++)); do
    result+=$delim
    result+=$phrase
  done
  echo "$result"
}

if [ $concat -eq 1 ]; then
  echo $(concatenate)
else
  for ((i=0; i<$count; i++)); do
    echo "$phrase"
  done
fi
