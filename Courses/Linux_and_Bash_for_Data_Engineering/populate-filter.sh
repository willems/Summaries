#!/usr/bin/env bash

echo "How many lines?"
read LINES

declare -a array=("Apple" "Pear" "Cherry")

COUNT=1
while [ $COUNT -le $LINES ]
do
	rand=$[ $RANDOM % 3 ]
	echo "$COUNT ${array[$rand]}" >> filter-file.txt
	((COUNT++))
done





# echo $(($RANDOM % 3))    cli
# Get particular line from text file
# sed '5!d' file
# awk 'NR==5' file

# Count occurrences
# grep -c -e Apple -e Pear filter-file.txt
# or 
# grep -e Apple -e Pear filter-file.txt | wc -l

# count not meeting criteria
# grep -c -v Apple filter-file.txt
