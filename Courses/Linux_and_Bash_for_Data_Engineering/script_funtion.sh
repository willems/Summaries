#!/usr/bin/bash

add() {
	num1=$1
	num2=$2
	result=$((num1+num2))
	echo $result
}

output=$(add 1 2)
echo "result: $output"