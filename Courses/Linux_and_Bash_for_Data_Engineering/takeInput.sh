#!/usr/bin/env bash

# if [ -z "$FILE" ]; then
#	echo "no file argument is passed" && exit 1;
# fi


if [[ $# -eq 0 ]]; then
	echo "At least one argument is required!" && exit 1;
fi

for var in "$@";
do
	cat "$var"
	echo -e "\nTimestamp last modification of $var:"
	stat -c %y "$var"
done
