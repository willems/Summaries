CREATE TABLE
  make (
    id SERIAL,
    name VARCHAR(128) UNIQUE,
    PRIMARY KEY (id)
  );

CREATE TABLE
  model (
    id SERIAL,
    name VARCHAR(128),
    make_id INTEGER REFERENCES make (id) ON DELETE CASCADE,
    PRIMARY KEY (id)
  );

insert into
  make (name)
VALUES
  ('GMC');

insert into
  make (name)
VALUES
  ('Toyota');

select
  *
from
  make;

insert into
  model (name, make_id)
VALUES
  ('Savana 2500 2WD Conversion (cargo)', 1);

insert into
  model (name, make_id)
VALUES
  ('Savana 3500 2WD (Passenger)', 1);

insert into
  model (name, make_id)
VALUES
  ('Savana 3500 2WD (cargo)', 1);

insert into
  model (name, make_id)
VALUES
  ('Cab/Chassis', 2);

insert into
  model (name, make_id)
VALUES
  ('Cab/Chassis 2WD', 2);

select
  *
from
  model;

SELECT
  make.name,
  model.name
FROM
  model
  JOIN make ON model.make_id = make.id
ORDER BY
  make.name
LIMIT
  5;