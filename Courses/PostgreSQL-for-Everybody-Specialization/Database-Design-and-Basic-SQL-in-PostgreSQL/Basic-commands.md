Clients:
- pgAdmin or DBeaver( https://dbeaver.io/ ) or Jupyter Notebook
- psql

psql -U postgres

\l     # from psql promt list all the databases


CREATE USER pg4e WITH PASSWORD 'secret';
CREATE DATABASE people WITH OWNER 'pg4e';

\q    # get out of session


psql people pg4e  # connect to database

\dt  # show tables in database

DROP TABLE users;

CREATE TABLE users(
    id SERIAL,
    name VARCHAR(128),
    email VARCHAR(128) UNIQUE,   # logical key
    PRIMARY KEY(id)
);

\d+ users   # show table schema od table users


## access course database first
Host:     pg.pg4e.com 
Port:     5432 
Database: pg4e_47777bca98 
User:     pg4e_47777bca98 
Password:  (hide/show copy) 
psql -h pg.pg4e.com -p 5432 -U pg4e_47777bca98 pg4e_47777bca98
password: pg4e_p_767dc2b686a11d2

\i lesson1.sql   # run sql in file


## access course database second

Host:     pg.pg4e.com 
Port:     5432 
Database: pg4e_8675309 
User:     pg4e_8675309 
Password: pg4e_p_422398745954





