create database course2;

use database course2;

CREATE TABLE account (
id SERIAL,
email VARCHAR(128) UNIQUE,
created_at DATE NOT NULL DEFAULT NOW(),
updated_at DATE NOT NULL DEFAULT NOW(),
PRIMARY KEY (id)
);

CREATE TABLE post (
id SERIAL,
title VARCHAR(128) UNIQUE NOT NULL, -- Will extend with ALTER
content VARCHAR(1024), -- fix to TEXT
account_id INTEGER REFERENCES account(id) ON DELETE CASCADE,
created at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
PRIMARY KEY (id)
);

— Allow multiple comments
CREATE TABLE comment (
id SERIAL,
content TEXT NOT NULL,
account_id INTEGER REFERENCES account(id) ON DELETE CASCADE,
post_id INTEGER REFERENCES post(id) ON DELETE CASCADE,
created at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
updated at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
PRIMARY KEY (id)
);

CREATE TABLE fav (
id SERIAL,
oops TEXT, -- Will remove later with ALTER
post_id INTEGER REFERENCES post(id) ON DELETE CASCADE,
account_id INTEGER REFERENCES account (id) ON DELETE CASCADE,
created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
UNIQUE(post_id, account_id),
PRIMARY KEY (id)
);

\d+ fav
--ALTER TABLE

ALTER TABLY post ALTER COLUMN content TYPE TEXT;

ALTER TABLE fav DROP COLUMN oops;

ALTER TABLE fav ADD COLUMN howmuch INTEGER;

-- Read SQL Commands fom a script

-- Download https://www.pg4e.com/lectures/03-Techniques-Load.sql

\i 03-Techniques-Load.sql



=============
-- Download https://www.pg4e.com/lectures/03-Techniques.csv
-- Zap,A
-- Zip,A
-- 0ne,B
-- Two, B

DROP TABLE IF EXISTS xy_raw;
DROP TABLE IF EXISTS y;
DROP TABLE IF EXISTS xy;

CREATE TABLE xy_raw(x TEXT, y TEXT, y_id INTEGER);
CREATE TABLE y (id SERIAL, PRIMARY KEY(id), y TEXT);
CREATE TABLE xy(id SERIAL, PRIMARY KEY(id), x TEXT, y_id INTEGER, UNIQUE(x,y_id));

\d xy_raw
\d+ y

COPY xy_raw(x,y) FROM '03-Techniques.csv' WITH DELIMITER ',' CSV;

SELECT DISTINCT y from xy_raw;

INSERT INTO y (y) SELECT DISTINCT y FROM xy_raw;

UPDATE xy_raw SET y_id = (SELECT y.id FROM y WHERE y.y = xy_raw.y);
SELECT * FROM xy_raw;

INSERT INTO xy (x, y_id) SELECT x, y_id FROM xy_raw;

SELECT * FROM xy JOIN y ON xy.y_id = y.


-- opdracht week 2  eerste

\copy track_raw (title, artist, album, count, rating, len) from 'library.csv' WITH DELIMITER ',' CSV;


-- opdracht week 2  tweede

DROP TABLE IF EXISTS unesco_raw;
CREATE TABLE unesco_raw
 (name TEXT, description TEXT, justification TEXT, year INTEGER,
    longitude FLOAT, latitude FLOAT, area_hectares FLOAT,
    category TEXT, category_id INTEGER, state TEXT, state_id INTEGER,
    region TEXT, region_id INTEGER, iso TEXT, iso_id INTEGER);

DROP TABLE IF EXISTS category;
CREATE TABLE category (
  id SERIAL,
  name VARCHAR(128) UNIQUE,
  PRIMARY KEY(id)
);

insert into category (name) select distinct category from unesco_raw order by category;

create table iso(                                                      
id SERIAL,
name char(2),
primary key(id)
);

insert into iso (name) select distinct iso from unesco_raw order by iso;

create table state (
id SERIAL,
name varchar(128),
primary key(id)
);

insert into state (name) select distinct state from unesco_raw order by state;

create table region (                                                            
id SERIAL,
name varchar(128),
primary key(id));

insert into region (name) select distinct region from unesco_raw order by region;

create table unesco(id SERIAL, name varchar(256), description text, justification TEXT, longitude FLOAT ,latitude FLOAT, area_hectares FLOAT, year INTEGER, state_id INTEGER, region_id INTEGER, iso_id INTEGER, category_id INTEGER, primary key(id));

update unesco_raw a set category_id = (select id from category where name = a.category);

update unesco_raw a set region_id = (select id from region where name = a.region);

update unesco_raw a set iso_id = (select id from iso where name = a.iso);

update unesco_raw a set state_id = (select id from state where name = a.state);


insert into unesco (name, description, justification, longitude, latitude, area_hectares, year, state_id, region_id,iso_id,category_id) select name, description, justification, longitude, latitude, area_hectares, year, state_id, region_id,iso_id,category_id from unesco_raw;


