DROP TABLE IF EXISTS album CASCADE;


CREATE TABLE
    album (id SERIAL, title VARCHAR(128) UNIQUE, PRIMARY KEY (id));


DROP TABLE IF EXISTS track CASCADE;


CREATE TABLE
    track (
        id SERIAL,
        title TEXT,
        artist TEXT,
        album TEXT,
        album_id INTEGER REFERENCES album (id) ON DELETE CASCADE,
        COUNT INTEGER,
        rating INTEGER,
        len INTEGER,
        PRIMARY KEY (id)
    );


DROP TABLE IF EXISTS artist CASCADE;


CREATE TABLE
    artist (id SERIAL, NAME VARCHAR(128) UNIQUE, PRIMARY KEY (id));

DROP TABLE IF EXISTS tracktoartist CASCADE;


CREATE TABLE
    tracktoartist (
        id SERIAL,
        track VARCHAR(128),
        track_id INTEGER REFERENCES track (id) ON DELETE CASCADE,
        artist VARCHAR(128),
        artist_id INTEGER REFERENCES artist (id) ON DELETE CASCADE,
        PRIMARY KEY (id)
    );


COPY track (title, artist, album, COUNT, rating, len)
FROM
    'library.csv'
WITH
    DELIMITER ',' CSV;


INSERT INTO
    album (title)
SELECT DISTINCT
    album
FROM
    track;


UPDATE track
SET
    album_id = (
        SELECT
            album.id
        FROM
            album
        WHERE
            album.title = track.album
    );


INSERT INTO
    tracktoartist (track, artist)
SELECT DISTINCT
    title,
    artist
FROM
    track
ORDER BY
    title,
    artist;


INSERT INTO
    artist (NAME)
SELECT DISTINCT
    (artist)
FROM
    track
ORDER BY
    artist;


UPDATE tracktoartist a
SET
    track_id = (
        SELECT
            id
        FROM
            track
        WHERE
            title = a.track
    );


UPDATE tracktoartist a
SET
    artist_id = (
        SELECT
            id
        FROM
            artist
        WHERE
            NAME = a.artist
    );


-- We are now done with these text fields
ALTER TABLE track
DROP COLUMN album;


ALTER TABLE track
DROP COLUMN artist;


ALTER TABLE tracktoartist
DROP COLUMN track;


ALTER TABLE tracktoartist
DROP COLUMN artist;


SELECT
    track.title,
    album.title,
    artist.name
FROM
    track
    JOIN album ON track.album_id = album.id
    JOIN tracktoartist ON track.id = tracktoartist.track_id
    JOIN artist ON tracktoartist.artist_id = artist.id
LIMIT
    3;