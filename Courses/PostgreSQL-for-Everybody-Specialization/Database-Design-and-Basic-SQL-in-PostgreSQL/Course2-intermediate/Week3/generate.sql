select random(), random(), trunc(random() * 1000);
select repeat('Neon ',5);
select generate_series(1,5);

select 'https://sql4e.com/neon/' ||
    trunc(random()*1000000) || repeat('Lemon',5) ||
    generate_series(1,5);

result:
?column?                         
---------------------------------------------------------
 https://sql4e.com/neon/282876LemonLemonLemonLemonLemon1
 https://sql4e.com/neon/862965LemonLemonLemonLemonLemon2
 https://sql4e.com/neon/620372LemonLemonLemonLemonLemon3
 https://sql4e.com/neon/336371LemonLemonLemonLemonLemon4
 https://sql4e.com/neon/723329LemonLemonLemonLemonLemon5


select k,a from 
    (select 'https://sql4e.com/neon/' ||
        trunc(random()*1000000) || repeat('Lemon',5) as a,
        generate_series(1,5) as k)
where k in (2, 4);

b-tree indexes are good for
 sorting
 exact lookups
 prefix lookups
 ranges

create table textfun(content TEXT);
create index textfun_b on textfun(content);   -- creates a b-tree index

select pg_relation_size('textfun'), pg_indexes_size('textfun');

INSERT INTO textfun (content)
SELECT (CASE WHEN (random() < 0.5)
THEN 'https://www.pg4e.com/neon/'
ELSE 'http://www.pg4e.com/LEMONS/'
END) || generate_series (100000,200000);

explain analyze select content from textfun where content ilike 'http://www.pg4e.com/LEMONS/100%';


