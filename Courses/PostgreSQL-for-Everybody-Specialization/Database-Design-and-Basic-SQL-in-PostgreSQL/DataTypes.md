---

markmap:
  colorFreezeLevel: 3

---

# Data Types
    
## Numeric

### Integer
- The SMALLINT type is generally only used
 if disk space is at a premium.
 Defaul ==INTEGER==
- **smaillint** [2 bytes; range -32768 to +32767]
- **integer** [4 bytes; range -2.147.483.648 to +2147483647]
- **bigint** [8 bytes]

### Arbitary Precision Number
- Recommended for storing monetary amounts and other
 quantities where exactness is required. eq NUMERIC(2, -3)
- **numeric(precision, scale)**
- **numeric(precision)**
- **numeric**

### Floating-Point Types (inexact)
- **real** [4 bytes] 7 digits accuracy
- **double** [8 bytes] 17 digits accuraccy

### Serial Types
- **smallserial** [2 bytes; range 1 to 32767]
- **serial** [4 bytes; range 1 to 2147483647]
- **bigserial** [8 bytes]

## Monetary Types
- **money** [8 bytes]
- SELECT '12.34'::float8::numeric::money;

## Character Types
- **character varying(n), varchar(n)**
- **character(n), char(n), bpchar(n)** [fixed-length, blank-padded]
- **bpchar** [variable unlimited length, blank-trimmed]
- **text** [variable unlimited length]

## Binary Data Types
- [bytea Hex, Escape](https://www.postgresql.org/docs/current/datatype-binary.html#DATATYPE-BINARY-BYTEA-ESCAPE-FORMAT)

## Date/Time Types
- **timestamp** [ (p) ] [ without time zone | with time zone ]
- **date** date (no time of day)
- **time** [ (p) ] [ without time zone | with time zone]
- **interval** [ fields ] [ (p) ]
- fields: YEAR, MONTH, YEAR TO MONTH etc

## Boolean Type
- **boolean** [1 byte; true or false]

## Enumerated Types
## Network Address Types
## XML types
## JSON Types
## Arrays
## Composite Types
## Range Types
## Geometric Types