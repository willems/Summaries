# Indexes

## B-tree
A B-tree is a tree data structure that keeps data sorted and allows searches, sequential access, insertions, and deletions in logarithmic amortized time. The Btree is optimized for systems that read and write large blocks of data. It is commonly used in databases and file systems.


## Hash
A hash function is any algorithm or subroutine that
maps large data sets to smaller data sets, called
keys. For example, a single integer can serve as an
index to an array (cf. associative array). The values
returned by a hash function are called hash values,
hash codes, hash sums, checksums, or simply hashes.
Hash functions are mostly used to accelerate table
lookup or data comparison tasks such as finding
items in a database

Hash funtions MD5, SHA1, SHA256


# Keys

## Primary key
Generally an **integer auto-increment field
**ID** is name of this fied; convention, but can be different between organisations

## Logical key
What the outside world is looking for eq emailadress
Never is this field also the primary key


## Foreign key
general an integer key pointing to a row primary key in another table 


## Database Normalization (3NF)

There is **tons** of database theory - way too much to understand without
excessive predicate calculus
- Do not replicate data. Instead, reference data. Point at data.
- Use integers for keys and for references.
- Add a special “key” column to each table, which you will make references to.