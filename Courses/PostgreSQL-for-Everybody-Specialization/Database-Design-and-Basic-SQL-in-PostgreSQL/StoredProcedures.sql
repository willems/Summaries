-- Stored Procedures
-- - A stored procedure is a bit of reusable code that runs inside of the database server
-- - Technically there are multiple language choices but just use "plpgsql"
-- - Generally quite non-portable
-- - Usually the goal is to to have fewer SQL statements

-- Stored Procedures
-- - You should have a strong reason to use a stored procedure
-- - Major performance problem
-- - Harder to test / modify
-- - No database portability
-- - Some rule that *must* be enforced

DROP table fav;
DROP table post;
DROP table account;

CREATE TABLE account (
    id SERIAL,
    email VARCHAR(128) UNIQUE,
    howmuch INTEGER DEFAULT 0,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
PRIMARY KEY (id)
);

insert into account (email) values ('aaa@company.com')
RETURNING *;

select * from account ;

update account SET howmuch = howmuch + 1, updated_at = NOW()
where id = 1 
RETURNING *;

-- Using a trigger for updated_at

-- this a an function
CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW() ;
    RETURN NEW;
    END
$$ LANGUAGE plpgsql;


-- This is the actual trigger and is using the function above.
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON account
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

select * from account;

UPDATE account SET howmuch=howmuch+1
WHERE id = 1
RETURNING *;


