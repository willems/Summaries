drop table users;

create table users (
    id SERIAL,
    name varchar(128),
    email varchar(128)
    PRIMARY KEY(id)
);