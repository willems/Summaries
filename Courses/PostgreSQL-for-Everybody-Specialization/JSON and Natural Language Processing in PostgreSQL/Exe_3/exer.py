import psycopg2
import hidden

# Load the secrets
secrets = hidden.secrets()

conn = psycopg2.connect(host=secrets['host'],
        port=secrets['port'],
        database=secrets['database'], 
        user=secrets['user'], 
        password=secrets['pass'], 
        connect_timeout=3)

cur = conn.cursor()

sql = 'DROP TABLE IF EXISTS pythonseq CASCADE;'
print(sql)
cur.execute(sql)

sql = 'CREATE TABLE pythonseq (iter INTEGER, val INTEGER);'
print(sql)
cur.execute(sql)

conn.commit()

value = 89224
for i in range(1,301) :
    sql = "insert into pythonseq (iter, val) values (%s, %s)"
    print(i, value)
    cur.execute(sql, (i, value))
    value = int((value * 22) / 7) % 1000000
    if i % 50 == 0:
        conn.commit()
        print(f"commited {i}")

conn.commit()

print('done')
       