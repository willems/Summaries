
```sql
CREATE TABLE Customer_transactions (
 Customer_id VARCHAR(40),
 txn_amout DECIMAL(38, 2),
 txn_type  VARCHAR(100))
PARTITIONED BY (txn_date STRING);



set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.dynamic.partition.mode=nonstrict
row_number() OVER() 
insert INTO customer_transactions PARTITION(txn_date)
select '122' as Customer_id, 2400 as txn_amout, 'Debit' as txn_type, '2019-12-01' as txn_date;

insert INTO customer_transactions PARTITION(txn_date)
select '124' as Customer_id, 200 as txn_amout, 'Credit' as txn_type, '2019-12-02' as txn_date;

insert INTO customer_transactions PARTITION(txn_date)
select '130' as Customer_id, 2110 as txn_amout, 'Credit' as txn_type, '2019-12-22' as txn_date;

insert INTO customer_transactions PARTITION(txn_date)
select '131' as Customer_id, 4110 as txn_amout, 'Debit' as txn_type, '2020-12-22' as txn_date;

select customer_id, txn_type, row_number() over (partition by txn_type order by txn_amout desc) as rank
from Customer_transactions




show partitions Customer_transactions

describe Customer_transactions

select version()
```
