# Timestamp

```sql
select unix_timestamp(); // => bigint
select unix_timestamp('2020-03-25 16:32:01');   // => bigint
```

## unix_timestamp(string date, string pattern)

```sql
select unix_timestamp(‘2020-03-25′,’yyyy-MM-dd’);
select unix_timestamp('16:39','HH:mm');
select unix_timestamp('2022-03-20 16:39','YYYY-DD-MM HH:mm'); // => bigint
```

## to_date(string timestamp)

```sql
select to_date('2020-03-25 16:32:01'); => 2020-03-25
```

## year(string date)

```sql
select quarter('2020-03-25 16:32:01');  => 1
select weekofyear('2020-03-25 16:32:01');
select year('2020-03-25');
select month('2020-03-25');
select day('2020-03-25');
select hour('2020-03-25 16:32:01')
select minute('2020-03-25 16:32:01')
select second('2020-03-25 16:32:01')
```

## datediff(string enddate, string startdate)

```sql
select datediff('2020-03-30', '2020-03-25');
```

## date_add(date |timestamp startdate, smallint |int days) and date_sub

```sql
select date_add('2020-03-25 16:32:01', 1);
select date_sub('2020-03-25 16:32:01', 1);
```

[Source](https://www.educba.com/hive-timestamp/)
