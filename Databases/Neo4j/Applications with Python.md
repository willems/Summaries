---
title: Applications with Python
updated: 2022-08-08 19:51:06Z
created: 2022-08-08 19:50:49Z
---
```python
from neo4j import GraphDatabase

driver = GraphDatabase.driver("neo4j://localhost:7687", auth=("neo4j", "neo"))
```

![Connection String](../../_resources/connection-string.png)

## Connection String

Required:

- Schema
- Initial server address
- port number (default 7687)

schemas:

- neo4j creates an unencrypted connection
- neo4j+s creates an encrypted connection and verifies certificate
- neo4j+ssc - Creates an encrypted connection no verification certificate

Variations of the bolt scheme can be used to connect directly to a single DBMS (within a clustered environment or standalone). This can be useful if you have a single server configured for data science or analytics.

- bolt creates an unencrypted connection directly to a single DBMS.
- bolt+s creates an encrypted connection directly to a single DBMS and verify the certificate.
- bolt+ssc creates an encrypted connection to directly to a single DBMS but will not attempt to verify the authenticity of the certificate.

**Which scheme string is right?**

You can verify the encryption level of your DBMS by checking the dbms.connector.bolt.enabled key in neo4j.conf.

Always use the neo4j+s scheme when connecting to Neo4j Aura

## Authentication Token

```python
auth = (username, password)
```

## Additional Driver Configuration (Optional)

Example

```python
GraphDatabase.driver(uri, auth=auth,
    max_connection_lifetime=30 * 60,
    max_connection_pool_size=50,
    connection_acquisition_timeout=2 * 60)
```
