---
title: Complex Networks
updated: 2021-10-04 17:28:56Z
created: 2021-10-04 15:37:12Z
latitude: 52.09370000
longitude: 6.72510000
altitude: 0.0000
---

![193bfaaed851ce4eef6d43f0a20c03a7.png](../../_resources/193bfaaed851ce4eef6d43f0a20c03a7.png)
# Node and Edge Characteristics
![c9a1ce3aec2e82148c02a5d70d096109.png](../../_resources/c9a1ce3aec2e82148c02a5d70d096109.png)
![a546793b2366edc935ce4a1d98db7f1e.png](../../_resources/a546793b2366edc935ce4a1d98db7f1e.png)

http://ks3329888.kimsufi.com/Intro2GraphTheoryAndComplexNetworksAnalysis/

![4d44a731b2dcebdb8e5210d5f6378b45.png](../../_resources/4d44a731b2dcebdb8e5210d5f6378b45.png)
1. Starting from a set of n vertices
2. Give two random vertices there is a probability P that they are linked together.

![cf910f508994db7a3d68732db2c383fb.png](../../_resources/cf910f508994db7a3d68732db2c383fb.png)
1. Generate a lattice
2. Nodes are initially linked to k closest neighbours
3. Apply a rewiring probability

![8cf3db7009d5518b37d9b0deb9b88dea.png](../../_resources/8cf3db7009d5518b37d9b0deb9b88dea.png)

![5362730add08cb32ca4de62973425703.png](../../_resources/5362730add08cb32ca4de62973425703.png)

![b685f0b119c20427c202a7143b227b60.png](../../_resources/b685f0b119c20427c202a7143b227b60.png)

![f2a731b0ccf8dc8c852572318d9d6bea.png](../../_resources/f2a731b0ccf8dc8c852572318d9d6bea.png)

![fe7d21e251098349149692c251f53dbc.png](../../_resources/fe7d21e251098349149692c251f53dbc.png)

![536ce7d9e4c324339ba03a53d7eaaec9.png](../../_resources/536ce7d9e4c324339ba03a53d7eaaec9.png)

![eb4e1a22065b0f7b00096573ca4f7cff.png](../../_resources/eb4e1a22065b0f7b00096573ca4f7cff.png)

![418daa8979836e67e544c4b275820715.png](../../_resources/418daa8979836e67e544c4b275820715.png)

![c30291ea3fd814cde29bbc5e2f275c3c.png](../../_resources/c30291ea3fd814cde29bbc5e2f275c3c.png)

![0ceb8df673446ebaaa7f858751a50669.png](../../_resources/0ceb8df673446ebaaa7f858751a50669.png)

[Network Analysis with Python and NetworkX Cheat Sheet](https://cheatography.com/murenei/cheat-sheets/network-analysis-with-python-and-networkx/)

[Network Science Book](http://networksciencebook.com/)

[The Colorado Index of Complex Networks](https://icon.colorado.edu/#!/)

[Stanford Large Network Dataset Collection](https://snap.stanford.edu/data/)

