---
title: Graph Theory
updated: 2022-07-18 09:46:19Z
created: 2021-10-02 17:39:39Z
latitude: 52.09370000
longitude: 6.72510000
altitude: 0.0000
---

## Euler Path

uses every single egde once; no repeats
No need to have start en end vertex the same.
**0 or 2 odd degree vertices, the rest even**
![0dbbbe727224fc349edc14517e3b93c9.png](../../_resources/0dbbbe727224fc349edc14517e3b93c9.png)

## Euler Circuit

Every edge once; no repeats
Start and end vertex are the same
**All vertices must have an even degree**
Multiple Euler Circuits are possible and depends on the starting direction.
![3f0188b20328cc489d50b321df8d6bc7.png](../../_resources/3f0188b20328cc489d50b321df8d6bc7.png)

## Fleury's Algorithme

Find an Euler Circuit, starting at vertex A
![2e79a268b8d9ef0e61db6ea008bafd7d.png](../../_resources/2e79a268b8d9ef0e61db6ea008bafd7d.png)
Remove the edge take to go the next vertex, but a disconnected graph is not allowed. Deleting an edge prevents backtracking.
![a99bbee6299b41776e45d4af26288c05.png](../../_resources/a99bbee6299b41776e45d4af26288c05.png)

## Eulerization

Duplicate egdes so a Euler cirquit is in the graph, from and to vertices with odd degree. No new edges. Minimize duplication of edges. Multiple solutions possible.
![8cc8d717ee428ffeb48a396d804b91de.png](../../_resources/8cc8d717ee428ffeb48a396d804b91de.png)

## Hamiltonian Circuit

Visit every vertex once, with no repeats,
![91c43645ccabeee0047757424141fb5d.png](../../_resources/91c43645ccabeee0047757424141fb5d.png)
Minimun cost Hamiltonian Circuit == (Traveling Salesman Problem)
Possible method: Brute Force
Optimal, bit not efficient
![5815ff14a02f76a5118a6ef8f86ed048.png](../../_resources/5815ff14a02f76a5118a6ef8f86ed048.png)

## Complete Graph

All vertices are connected with all the others.
If n vertices then (n-1)!/2 different hamiltonian circuits

Better then Brute Force the get the shortest circuit  are heuristic methods;

- **Nearest Neighbor Algorithm** (not optimal)
  Start at A and take at every vertex the edge with the lowest weight. (Greedy, doesn't look ahead)
  ![beb7e37bd0e389910f37d2ef559a1900.png](../../_resources/beb7e37bd0e389910f37d2ef559a1900.png)
- **Repeated Nearest Algorithm**
  Same as Nearest Neighbor Algorithm but repeat for every vertex and select the lowest cost.
  ![89ef06b3ace7055254b5a4d1b2db1663.png](../../_resources/89ef06b3ace7055254b5a4d1b2db1663.png)
- **Sorted Edge Algorithm** (not optimal)
  Add cheapest up, unless:
  - a mini circuit (a curcuit that doesn't include all vertices)
  - no vertex with a degree 3

AD = 1
AC = 2
AB = 4 (not because of degree 3 rule)
CD = 8 (not because creates mini circuit)
BD = 9
BC = 13 
Most optimal is ADBCA = 25
![b3fabbeba3f22c574c31078c58c5dbe8.png](../../_resources/b3fabbeba3f22c574c31078c58c5dbe8.png)

## Hamiltonian Path

Visit every vertex once, with no repeats,
![c34ed20473a52dda3d546240f3b6e52c.png](../../_resources/c34ed20473a52dda3d546240f3b6e52c.png)

## Kruskal's Algoritme (optimal and efficient)

Similar to Sorted Edge Algorithm, but no circuit.
Minimim cost spanning tree == every vertex is connected to an other vertex
![b55dc1138ff9d80431c97102da97c98c.png](../../_resources/b55dc1138ff9d80431c97102da97c98c.png)
Add cheapest up, unless it creates a circuit.
![c1e5b885c1aead4ff5700b0a10cc0302.png](../../_resources/c1e5b885c1aead4ff5700b0a10cc0302.png)








