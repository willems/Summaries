# Admin tasks

## Load a new dump

1. create the new database and use it

    ```
    CREATE DATABASE <database_name>
    :use <database_name>
    :dbs     // list all available databases
    ```

2. offline the new database

    ```
    STOP DATABASE <name>
    ```

3. connect to running docker container

    ```
    docker exec -it neo4jserver bash
    ```

4. load the dump

    ```
    neo4j-admin load --from=/data/dumps/filename.dump --database=<name> --force
    ```

5. set the correct permissions for the new database

    ```
    chown -R neo4j:neo4j /var/lib/neo4j/data/transactions/<name>
    chown -R neo4j:neo4j /var/lib/neo4j/data/databases/<name>
    ```

6. start the database

    ```
    START DATABASE <name>
    ```

## neo4j configuration file neo4j.conf

dbms.default_database
