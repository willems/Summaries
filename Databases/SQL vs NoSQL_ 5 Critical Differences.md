# SQL vs NoSQL: 5 Critical Differences

[source](https://www.integrate.io/blog/the-sql-vs-nosql-difference/)

The five critical difference between SQL vs NoSQL are:

|SQL|NO-SQL|
|--|--|
|SQL databases are relational|NoSQL databases are non-relational|
|SQL databases use structured query language and have a **predefined schema**|NoSQL databases have **dynamic schemas** for unstructured data|
|SQL databases are **vertically scalable**|NoSQL databases are **horizontally scalable**|
|SQL databases are **table-based**|NoSQL databases are **document, key-value, graph, or wide-column stores**|
|SQL databases are better for **multi-row transactions**| NoSQL is better for **unstructured data like documents or JSON**|
|||

<hr/>

# Vertical Scaling

- More disk drives or disk arrays/RAID
- More processors
- More memory
- Swithc from spinning to solid state drives
    - Modern SSD drives have scatter/gather
- Has been solidly successful over the years

<hr/>

# ACID

[source](https://encyclopedia.pub/entry/35885)
- A = **Atomicity**
    Atomicity guarantees that each transaction is treated as a single "unit", which either succeeds completely, or fails completely: if any of the statements constituting a transaction fails to complete, the entire transaction fails and the database is left unchanged.
- C = **Consistency**
    a transaction can only bring the database from one valid state to another, maintaining database invariants: any data written to the database must be valid according to all defined rules, including constraints, cascades, triggers, and any combination thereof.
- I = **Isolation**
    depending on the method used, the effects of an incomplete transaction might not even be visible to other transactions.
- D = **Durability**
    once a transaction has been committed, it will remain committed even in the case of a system failure

<hr/>

# BASE

![BASE](https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fwww.cohesity.com%2Fwp-content%2Fnew_media%2F2020%2F09%2Fstrict-vs-eventual-consistency-2.jpg&sp=1705313745T71d7f9bf0427e7a57dabb7507a6add6245bcb7f5a8db8dede3940a23875379cd)

- B = **Basically**
    reading and writing operations are available as much as possible
- A = **Available**
- S = **Soft state**
    without consistency guarantees, after some amount of time, we only have some probability of knowing the state, since it might not yet have converged
- E = **Eventual consistency**
    If we execute some writes and then the system functions long enough, we can know the state of the data; any further reads of that data item will return the same value

## The basic principles of BASE DBMS
- Everything is distributed - fast network
- No locks (*)
- Lots of fast / small memory CPUs
- Lots of disks
- Indexes follow data shards
- Documents not rows / columns
- Schema on read - not schema on write(*)

## What is sharding?
[Source sharding](https://www.techtarget.com/searchoracle/definition/sharding)
Sharding is a type of database partitioning that separates large databases into smaller, faster, more easily managed parts. These smaller parts are called data shards. The word shard means "a small part of a whole."

### Horizontal and vertical sharding
- Horizontal sharding. When each new table has the same schema but unique rows,
- Vertical sharding. When each new table has a schema that is a faithful subset of the original table's schema

![](https://cdn.ttgtmedia.com/rms/onlineimages/benefits_of_horizontal_and_vertical_sharding-h.png)

## JSON Ascending
- JSON is a great way to represent / move / store / tructured data
- Fast parsers in every programming language
- Easily compressed to save storage and transfer

## Open Source NoSQL databases
- CouchDB (2008)
    - Cluster Of Unreliable Commodity Hardware
- MongoDB - 2009
    - Distributed JSON storage
- Cassandra - 2008
    - From FaceBook
    - Also Apache Hadoop - Map / Reduce
- ElasticSearch - 2010
    - Initially full text search Apache Lucene
    - Evolved into JSON database
    - Only inverted indexes

## Proprietary / Software AS a Service (SASS) NoSQL Databases
- Amazon DynamoDB
    - Backed the Amazon catalog
- Google BigTable
    - Stored Google's copy of the web
- Azure Table Storage
    - Catching up

<hr/>

# CAP theorem

![CAP](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/CAP_Theorem_Venn_Diagram.png/220px-CAP_Theorem_Venn_Diagram.png)

- C = **Consistency**
    Every read receives the most recent write or an error
- A = **Availability**
    Every request receives a (non-error) response, without the guarantee that it contains the most recent write
- P = **Partition tolerance**
    The system continues| ACID | BASE|
|----|----| to operate despite an arbitrary number of messages being dropped (or delayed) by the network between nodes.

<hr/>

# Database Software

| ACID | BASE|
|:----:|:----:|
|Oracle|Mongo|
|PostgreSQL|Casandra|
|MySQL|BigTable|
|SQLite||
|SQLServer||
|||

<hr/>

# Database Comprimises

| ACID | BASE|
|----|----|
|Serial Integer keys|GUID - Global Unique ID's|
|Transactions|Design for stale data in application|
|UNIQUE Constrains|Application post-check and resolve|
|"One perfect SQL Statement|Retrieve and throw away|
|||

<hr/>

# Kind databases
![Kind](https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fsubstackcdn.com%2Fimage%2Ffetch%2Ff_auto%2Cq_auto%3Agood%2Cfl_progressive%3Asteep%2Fhttps%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F31662535-c0e6-43ee-adcb-c0349a431368_3141x2685.jpeg&sp=1705314234T7c081427b0aa6a65023f3366e31c9003540e7dce3e1383bcc31bff550e11e94b)

<hr/>

# Tuning a relational database
- add what are called read-only replicas
- inserts/updates on master database
- read on replica's

![](https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fwww.scylladb.com%2Fwp-content%2Fuploads%2Fdatabase-replication-diagram.png&sp=1705315627T176e5b7f706fa8ffc8d1d3b536bdaf243886ea3bc047aa1017975b4dbccf9edd)

- Multi-master is where you have two master databases but
because the master database has the responsibility of sort of putting a block on all transactions on the way until the transaction in flight really completes, there's a lot of coordination between the masters. (complicated)

<hr/>


![](https://www.startpage.com/av/proxy-image?piurl=https%3A%2F%2Fmiska.co.in%2Fwp-content%2Fuploads%2F2021%2F11%2FNoSQL-Databases-type-1024x664.jpg&sp=1705316116Tf907735e5faa7815cff74ee9c7ef4e027e4ea07180195d04bca706f04bd14978)

<hr/>

# Being BASE-Like in ACID RDBMS
- Do not normalize — **Replicate**
- Don't use SERIAL - **use UUID**
- Columns are for indexing
- Do not use foreign keys or don't mark them as such
- Design your schema / indexes to enable reading a single row on query
- Use software migrations instead of ALTER
- Query for records by primary key or by indexed column
- Do not use JOINs
- Do not use aggregations (other then COUNT)

[Source](https://www.wix.engineering/post/scaling-to-100m-mysql-is-a-better-nosql)