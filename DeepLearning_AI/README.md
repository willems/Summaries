---
title: README
updated: 2022-05-16 18:26:58Z
created: 2022-05-16 17:43:56Z
---

# Deep Learning Specialization Course Notes

This is the notes of the [Deep Learning Specialization](https://www.coursera.org/specializations/deep-learning) courses offered by [deeplearning.ai](https://www.deeplearning.ai/) on Coursera.

Introduction from the specialization page:

>In five courses, you will learn the foundations of Deep Learning, understand how to build neural networks, and learn how to lead successful machine learning projects. You will learn about Convolutional networks, RNNs, LSTM, Adam, Dropout, BatchNorm, Xavier/He initialization, and more. You will work on case studies from healthcare, autonomous driving, sign language reading, music generation, and natural language processing. You will master not only the theory, but also see how it is applied in industry. You will practice all these ideas in Python and in TensorFlow, which we will teach.

*The Specialization consists of five courses*:

- [Course 1: Neural Networks and Deep Learning](:/8b8d24c8270944829c58a2071481e8b7)
  - [Week 1: Introduction to Deep Learning](:/8b8d24c8270944829c58a2071481e8b7#week-1-introduction-to-deep-learning)
  - [Week 2: Neural Networks Basics](:/8b8d24c8270944829c58a2071481e8b7#week-2-neural-networks-basics)
  - [Week 3: Shallow Neural Networks](:/8b8d24c8270944829c58a2071481e8b7#week-3-shallow-neural-networks)
  - [Week 4: Deep Neural Networks](:/8b8d24c8270944829c58a2071481e8b7#week-4-deep-neural-networks)
- [Course 2: Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization](:/eae2031754c04c308d51a81b8b0e4b1f)
  - [Week 1: Practical aspects of Deep Learning](:/eae2031754c04c308d51a81b8b0e4b1f#week-1-practical-aspects-of-deep-learning)
  - [Week 2: Optimization algorithms](:/eae2031754c04c308d51a81b8b0e4b1f#week-2-optimization-algorithms)
  - [Week 3: Hyperparameter tuning, Batch Normalization and Programming Frameworks](:/eae2031754c04c308d51a81b8b0e4b1f#week-3-hyperparameter-tuning-batch-normalization-and-programming-frameworks)
- [Course 3: Structuring Machine Learning Projects](:/955c977bae464e6aa23cd6f94a8461ed)
  - [Week 1: ML Strategy (1)](:/955c977bae464e6aa23cd6f94a8461ed#week-1-ml-strategy-1)
  - [Week 2: ML Strategy (2)](:/955c977bae464e6aa23cd6f94a8461ed#week-2-ml-strategy-2)
- [Course 4: Convolutional Neural Networks](:/1c1155ef678f4b41a1b0aa6fd36eabad)
  - [Week 1: Foundations of Convolutional Neural Networks](:/1c1155ef678f4b41a1b0aa6fd36eabad#week-1-foundations-of-convolutional-neural-networks)
  - [Week 2: Classic Networks](:/1c1155ef678f4b41a1b0aa6fd36eabad#week-2-classic-networks)
  - [Week 3: Object detection](:/1c1155ef678f4b41a1b0aa6fd36eabad#week-3-object-detection)
  - [Week 4: Special applications: Face recognition & Neural style transfer](:/1c1155ef678f4b41a1b0aa6fd36eabad#week-4-special-applications-face-recognition--neural-style-transfer)
- [Course 5: Sequence Models](:/8c3f644598994759a0d60b2a12997e60)
  - [Week 1: Recurrent Neural Networks](:/8c3f644598994759a0d60b2a12997e60#week-1-recurrent-neural-networks)
  - [Week 2: Natural Language Processing & Word Embeddings](:/8c3f644598994759a0d60b2a12997e60#week-2-natural-language-processing--word-embeddings)
  - [Week 3: Sequence models & Attention mechanism](:/8c3f644598994759a0d60b2a12997e60#week-3-sequence-models--attention-mechanism)



[fancy-course-summary]: https://www.slideshare.net/TessFerrandez/notes-from-coursera-deep-learning-courses-by-andrew-ng
[math-html]: https://www.toptal.com/designers/htmlarrows/letters/