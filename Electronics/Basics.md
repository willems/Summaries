# Summary Electricity basics

### Electric current

- Atoms that let electric current flow easily are called conductors (geleiders), whereas atoms that
don’t let current flow easily are called (isolatoren)
- atoms consist of neutrons, protons, and electrons.
- nucleus: neutrons and protons in middle of atom.
- around nuclues is a electron cloud.
- number of protons is nuclues is the atomic number (1 = hydrogen, 2 = lithium ...29 = copper)
- import, for electronics, are the electrons, becaus they're the source of electric current (elektrische stroom).
- atoms have same number of electrons and protons. If not the same number then there is **electric charge**
when the same then **neutral**
- Charge can be one of two polarities:
  - negative or positive.
  - Electrons have a negative polarity, while protons
have a positive polarity.
- electrons and protons are attracted to each other, but electrons repel other electrons and protons repel other protons.
- strength of an electric current is measured with a unit called the **ampere A or mA represented as I** (intensity): how many electrons flow past a certain point in one second.
- to move the electrons in one direction electromotive force (EMF) is needed, called **voltage (E)**
- A voltage is nothing more than a difference in charge between two places and creates the potential for a current to flow.
- Voltages can be considered positive or negative but only when compared with some reference point.
- Modern electronic circuits are almost always described in terms of conventional current (pos -> neg)
- An electric current that flows continuously in a single direction is called a **direct current DC**; the voltage in DC must be constant.
- **Alternate current AC**, voltage periodical reverses itself 50-60 time per second. The current wiggle back and forth. AC is most efficient when current transmitting over long distances.
- **power P in watts W** is the work done by a electric circuit. ie burning a light or heat a hairdryer. This is called **dissipate**
- P = E x I [power (P) = voltage (E) times current (I)]
  - P = 10V x 0.1A = 1W
  - I = 100W / 177V = 0.885A
- electrical components such as resistors,transistors, capacitors, and integrated circuits all have maximum power ratings.

### electric circuit

- (elektrische schakeling) is a closed loop made of conductors and other electrical elements through which electric current can flow
- The best conductors are the metals silver, copper, and aluminum.
- all circuits must obey the basic principle of a closed loop
![kabel](images/kabel.png)

### amplifier

converts the small electrical signal that comes from the source into a much larger electrical signal that, when sent to the speaker or headphones, can be heard.

### digital electronics

is concerned with manipulating data in the binary language of zeros and ones.

### circuit board

 populated with components that make the circuit: resistors, capacitors, diodes, transistors, and integrated circuits. On the backside are the conductors.

 ![circuit board](images/Circuit_board.png)

### components
- resistors: resist the flow of electric current; unit in ohms (Ω)
- capacitors: store electric charge; unit in farads (F)
  - materials either ceramic, tantalum, or electrolytic
- diodes: allow current to flow in one direction only
  - two terminals, called the anode and the cathode. Current flows from the anode to the cathode, when positive voltage is applied.
- light-emitting diodes (LEDs): emit light when current flows through them.
- transistors: is a three-terminal device in which a voltage applied to one of  the terminals (called the base) can control current that flows across the other two terminals (called the collector and the emitter).
- integrated circuits (ICs): are a collection of electronic components that are fabricated on a single piece of silicon.



