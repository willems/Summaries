# Docker

This container runs in background

```bash
docker run -d -it --gpus all -name pytorch-container pytorch/pytorch:latest
docker run -d -it --rm -v $(pwd):/src --gpus all -name pytorch-container pytorch/pytorch:latest
docker run hello-world
```

## Test GPU

```python
>>> import torch
>>> print(torch.cuda.is_available())
```

The latter removes the docker instance when stopped and alse has a volume

## Connect to running container

```bash
docker exec -it <container-name> bash
docker exec -it <container-name> bash -c "cat aap"
```

## Stop and start an execistion container

```bash
docker stop <container-name>
docker start <container-name>
```

### docker ipaddress or running container

```bash
docker inspect <container-name or id> | grep '"IPAddress"' | head -n 1
```

### Run Postgresql daemon and psql attach

```bash
docker run --name my-postgres -e POSTGRES_PASSWORD=qw12aap -d postgres:latest
docker exec -it my-postgres psql -h localhost -U postgres -d postgres
```

inside: psql -U postgres -h my-postgres

### docker files

```dockerfile
FROM python:latest
RUN pip3 install numpy
CMD python3 /src/hello-world.py
```

## docker volumes
```bash
docker volume create volume-name   # name volums
docker volume rm volume-name
docker volume inspect volume-name

docker volume ls
docker volume prune  # remove unused volumes
docker run -v /data nginx  # anonymous volume
```
Also share volumes between multiple containers by using the same volume name



### docker networks

```bash
docker network create net_1  ## named network

docker run --rm -d --net net_1 --name my_py -v $(pwd):/src  python:latest python3 /src/run.py

docker run --rm -it --net net_1 alpine:latest /bin/bash

docker network create net_2
docker run --rm --name my-postgres --network net_2 -e POSTGRES_PASSWORD=qw12aap -d postgres:latest
docker run -it --rm --name my_postgre2 --network net_2  postgres:latest /bin/bash
```

### Docker Compose; Container Network is created automatically

``` docker-compose
services:
    alpine:
        image: alpine:latest
        command: echo "hello from alpine"
        restart: always
```

```docker-compose
services:
     python:
             image: python:latest
             container_name: my_py
             volumes:
                     - storage_py:/src
             command: python3 /src/run.py
             restart: always
     postgres:
             image: postgres:latest
             container_name: my_post
             environment:
                     - e POSTGRES_PASSWORD=qw12aap
             volumes:
                - storage:/var/lib/postgresql/data
             restart: always
volumes:
     storage_py:
     storage:
             driver: local
             driver_opts:
                type: none
                device: /data/db_data
                o: bind
```

[How To Remove Docker Images, Containers, and Volumes](https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes)

[How to Troubleshoot “Cannot Connect to the Docker Daemon” Errors](https://www.howtogeek.com/devops/how-to-troubleshoot-cannot-connect-to-the-docker-daemon-errors/)

Location of names-volumes:  /var/lib/docker/volumes
