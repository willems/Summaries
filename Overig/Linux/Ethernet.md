---
title: Ethernet
updated: 2022-04-27 17:47:40Z
created: 2021-05-04 14:58:11Z
---

# Ethernet

## Change mac address

macchanger <device>  -r

## local ip address

hostname -I

## public ip address

curl ipinfo.io/ip

## scan local network

sudo arp-scan --interface=enp4s0 --localnet

```bash
sudo service network-manager restart
sudo vi [/etc/dhcp/dhclient.conf](file:///etc/dhcp/dhclient.conf)
```

edit "prepend domain-name-servers" for DNS servers

## Monitor wifi

wavemon

## available wifi networks

nmcli connection show

## which DNS server in use

( nmcli dev list || nmcli dev show ) 2>/dev/null | grep DNS
nm-tool | grep DNS

[Unbound](https://aacable.wordpress.com/2019/12/10/short-notes-for-unbound-caching-dns-server-under-ubuntu-18/)

```bash
sudo vi dns_servers.conf
```

enter:

[Resolve]
DNS=76.76.2.3 76.76.10.3

```bash
sudo systemctl restart systemd-resolved
```
