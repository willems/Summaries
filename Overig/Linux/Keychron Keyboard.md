---
title: Keychron Keyboard
updated: 2022-04-27 17:37:12Z
created: 2022-04-27 17:36:57Z
---

https://gist.github.com/andrebrait/961cefe730f4a2c41f57911e6195e444

[/dev/schnouki &ndash; How to use a Keychron K2/K4 USB keyboard on Linux](https://schnouki.net/post/2019/how-to-use-a-keychron-k2-usb-keyboard-on-linux/)