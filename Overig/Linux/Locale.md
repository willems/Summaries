---
title: Locale
updated: 2021-05-04 14:58:11Z
created: 2021-05-04 14:58:11Z
---

# Locale

sudo dpkg-reconfigure locales

- select en_US.UTF-8
- select en_US.UTF-8
- type "locale" to check again
