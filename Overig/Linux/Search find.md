---
title: Search find
updated: 2021-05-04 14:58:11Z
created: 2021-05-04 14:58:11Z
---

# Search find

find /. -name 'toBeSearched.file' 2>&1 | grep -v 'Permission denied'
