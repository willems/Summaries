---
title: Services
updated: 2022-04-27 17:35:25Z
created: 2021-05-04 14:58:11Z
---

# Services

## list all services

service --status-all

## start - stop - pauze services

sudo service <package name> <start | off | pauze | ....

## List services

systemctl list-unit-files

## Statup time

systemd-analyze blame


[set numa parameters](https://askubuntu.com/questions/1379119/how-to-set-the-numa-node-for-an-nvidia-gpu-persistently)