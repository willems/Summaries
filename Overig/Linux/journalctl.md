---
title: journalctl
updated: 2021-09-11 08:09:53Z
created: 2021-09-11 08:00:22Z
latitude: 52.09370000
longitude: 6.72510000
altitude: 0.0000
---

# only errors
`journalctl --utc`

# vie emergency system messages
`journalctl -p 0`

# Error codes:
0: emergency
1: alerts
2: critical
3: errors
4: warning
5: notice
6: info
7: debug

# shows all messages with priority 2, 1 and 0
`journalctl -p 2`

# errors at boot time
`journalctl --list-boots`

# errors with explanation
`journalctl -xb -p 3`

# show log from specific moment in time
`journalctl --since "2020-12-04 06:00:00"`
`journalctl --since yesterday`

# kernel messages
`journalctl -k`

# Network messages
`journalctl -u NetworkManager.service`

# list of all services
`systemctl list-units --type=service`

# messages of an application
`journalctl /usr/bin/docker --since today`


[Source](https://www.debugpoint.com/2020/12/systemd-journalctl/)