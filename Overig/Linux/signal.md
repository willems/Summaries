# Signal

You need to set both LANG (for spellchecking) and LANGUAGE (for interface language) variables.

Best is to create a custom signal-desktop.desktop shortcut in ~/.local/share/applications (this might be OS- or Desktop Environment-specific) so that it does not get overridden after every update.

Start by copying /usr/share/applications/signal-desktop.desktop to ~/.local/share/applications, then modify the Exec line to include your language, e.g.,

```bash
Exec=env LANG=nl_NL.UTF-8 LANGUAGE=nl_NL /opt/Signal/signal-desktop --no-sandbox %U
```
