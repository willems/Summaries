---
title: ssh
updated: 2021-05-04 14:58:11Z
created: 2021-05-04 14:58:11Z
---

# SSH keys

## check current

```bash
 for keyfile in ~/.ssh/id_*; do ssh-keygen -l -f "${keyfile}"; done | uniq
 ```

 Ed25519 is intended to provide attack resistance comparable to quality 128-bit symmetric ciphers.

 ```bash
 ssh-keygen -o -a 100 -t ed25519
 ```

 result

 ```bash
 ~/.ssh/id_ed25519
 ```

 [Source](https://blog.g3rt.nl/upgrade-your-ssh-keys.html)

## Server

ssh-keygen -f ~/.ssh/id_rsa -p -o -a 100   # don't use
ssh-keygen -t rsa -b 4096 -C "<your_email@domain.com>"

### on client

``` bash
ssh-copy-id remote_username@server_ip_address

# or if ssh-copy is not available

cat ~/.ssh/id_rsa.pub | ssh remote_username@server_ip_address "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys"

# or for register in a git repository

xclip -selection clipboard < ~/.ssh/id_ed25519.pub
```

[source](https://linuxize.com/post/how-to-setup-passwordless-ssh-login/)

[config ssh](https://ubuntuhandbook.org/index.php/2024/04/install-ssh-ubuntu-2404/)

[Codeberg docs](https://docs.codeberg.org/security/ssh-key/)