---
title: Remove string pdf
updated: 2021-10-04 13:26:02Z
created: 2021-10-04 13:21:30Z
latitude: 52.09370000
longitude: 6.72510000
altitude: 0.0000
---

```bash
sudo apt install pdftk
```

```bash
pdftk file.pdf output uncompressed.pdf uncompress
```

```bash
sed -i 's/RemoveString//g' uncompressed.pdf
```

```bash
pdftk uncompressed.pdf output changed.pdf compress
```