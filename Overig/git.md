# Git overview

## config

```bash
git config --global user.email "email address" # only for label modified by
git config --global user.name "name"
git config --global color.ui auto

# general
git config --add <section>.<keyname> <value>
```

## add to repository ssh autorisation
```bash
cat ~/.ssh/id_ed25519.pub
```

## Test autorisation
```bash
ssh -T git@github.com
```
response with 'yes' and hopely autorisation is OK


## Commit history

```bash
git log
git log --stat
git log --patch
git log --follow <file name>
git reflog
git checkout <commit hash>
git checkout master

# show difference
git diff [first-branch]...[second-branch]

git show [commit]
```

## ignore files, visual globally

```bash
touch .gitignore
~/.config/git/ignore  # global
```

[git ignore doc](https://git-scm.com/docs/gitignore)

remove from tracking

```bash
git rm --cached <filename>
```

## globally files to ignore, not visual for others

.gitignore file not required

```bash
.git/info/exclude
```

## Branches

```bash
# create new branch
git branche <new branche name>
# or
git checkout -b <new-branch-name>

# switch
git checkout <branche name>

# merge into current
git merge <branch name>

# delete branche, after possible commit
git branch -d <branch name>
```

[git branching doc](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)

## config in .git/

```bash
# add alias; different from bash alias
git config alias.st "status"

# usage
git st
```

## Synchronize changes

```bash

# Downloads all history from remote
git fetch

# Combines remote tracking branch into current local branch
git merge

# Uploads all local branch commits
git push

# update local with remote
git pull  == git fetch and git merge
```
