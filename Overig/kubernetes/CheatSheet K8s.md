---
title: CheatSheet K8s
updated: 2022-02-05 17:47:17Z
created: 2022-02-05 15:06:57Z
latitude: 51.86290000
longitude: 4.61910000
altitude: 0.0000
---

## minikube
`minikube start | status | stop | delete | pause | unpause | dashboard`

## Crud commands kubectl
`kubectl create deployment [name]`
`kubectl create deployment nginx-depl --image=nginx`
`kubectl edit deployment [name]`
`kubectl edit deployment nginx-depl`
`kubectl delete deployment [name]`

## Use configuration file for CRUD
`kubectl apply -f [file name]`
`kubectl delete -f [file name]`

## Status of different K8s components
`kubectl get nodes | pod | service | replicaset | deployment | all`
`kubectl get pod -o wide`   More columns with info
`kubectl get deployment [name] -o yaml`  current in yaml format (useful for debugging)

## Debugging pods
`kuberctl logs [pod name]`
`kubectl logs nginx-depl-{pod-name}`

`kubectl exec -it [pod name] -- bin/bash`
`kuberctl describe pod [pod name]`
`kuberctl describe pod nginx-depl-{pod name}`

## Configuration file Deployment & Service
Each configuration file has 3 parts
1. metadata
2. specification
3. status  (automatic generated)
	- desired state
	- Actual state
		- Both have to match. If not K8s knows something has to be fixed
		- status is updated continues
		- etcd is providing the info for status
![99bd030fd518baa2ca26a85b313e23f0.png](../../_resources/99bd030fd518baa2ca26a85b313e23f0.png)

![b138f59fcc154b98b815a2dc79b89003.png](../../_resources/b138f59fcc154b98b815a2dc79b89003.png)