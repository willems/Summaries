---
title: Ingress
updated: 2022-02-06 17:57:21Z
created: 2022-02-06 17:38:09Z
latitude: 51.86290000
longitude: 4.61910000
altitude: 0.0000
---

# install Ingress addon
Installs an additional pod
Starts automatically th K8s Mginx implimentation of Ingress controller
`minikube addons enable ingress'

Then create Ingress rule
![77e6428cf3d2bff19c7215e2915ca3c4.png](../../_resources/77e6428cf3d2bff19c7215e2915ca3c4.png)

or

![a6f83a73449bec9c309c2cab27c24f5e.png](../../_resources/a6f83a73449bec9c309c2cab27c24f5e.png)

SSL implementation
![47073357d36804a22ad703c90cb541ce.png](../../_resources/47073357d36804a22ad703c90cb541ce.png)

Take care!!!
![b97bd5343f507eb8529bc2c00d30084d.png](../../_resources/b97bd5343f507eb8529bc2c00d30084d.png)