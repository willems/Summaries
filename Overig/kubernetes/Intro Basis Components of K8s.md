---
title: Intro Basis Components of K8s
updated: 2022-02-05 15:10:02Z
created: 2022-01-29 15:33:56Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---

What is Kubernetes (K8s)

- container orchestration tool
- manages containers (not only Docker)
- different environments
    - physical
    - virtual
    - cloud

What problems solves Kubernetes:

- manages many containers with independent services, like micro services

Kubernetes features

- High Availability (no downtime)
- Scalability (high performance)
- Disaster recovery (backup and restore)

Kubernetes components (but many more)

- Node (physical or virtual)
    - node contains pods and per pod usually 1 application
    - pod is smallest unit of K8s
    - pod is abstraction over container
    - each pod gets its own ip-address
    - new ip-address on re-creation
    - ![aca7155a4c7e4f2b982378cbb2d37400.png](../../_resources/aca7155a4c7e4f2b982378cbb2d37400.png)
- Services
    - permanent ip-address
    - lifecycle of Pod and Service are not connected
    - ![ad9e649d35df7e77ce73056bbaf2cbb9.png](../../_resources/ad9e649d35df7e77ce73056bbaf2cbb9.png)
- External Service
    - opens the communication for external sources
    - internal service for eq database. Not accessible from outside
    - External request go first to Ingress (to route traffic)
    - ![25f981495ea54faccfe5ea58cd1f8b75.png](../../_resources/25f981495ea54faccfe5ea58cd1f8b75.png)
- ConfigMap & Secret
    - external configuration of application (eq URL of a database DB_URL = mongo-db-service)
    - secret credentials in Secret (like configMap but base64 encoded)
    - Application can read from ConfigMap and Secret
- Volumes
    - for persistent data
    - attach physical storage to a pod
    - local machine or remote storage (eq cloud and outside k8s cluster)
    - k8s does not manage data persistence
- Deployment and Stateful Set
    - Every Pod is replicated as specified in BluePrint
    - Service has 2 functionalities:
        - Permanent IP (so an other pod can connect of the pod dies)
        - load balancer
        - the pod is not created, but for every pod is specified (Blueprint) how many replicas are required
            - Blueprint is abstraction of Pods. Pods are an abstraction of Containers
            - In practice just working with Deployments
        - Stateful Set
            - Databases have state and can not be replicated
            - For stateful applications like databases
            - no database inconsistency can occur.
            - is not easy therefor advice to host databases outside K8s cluster