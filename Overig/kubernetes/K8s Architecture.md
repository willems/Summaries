---
title: K8s Architecture
updated: 2022-01-29 16:47:31Z
created: 2022-01-29 16:19:16Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---

Worker Machine in K8s cluster
- Nodes do the actual work
- each node has multiple Pots on it
- 3 process must be installed on every Node
	- Container Runtime (eq Docker)
	- Kubelet 
		- interacts with container and node
		- starts the Pod and the container inside
		- assigns resources from Node to the container
		- Communication between Nodes through Services
	- Kube Proxy
		- forwards the requests

Master Node (usally multple master nodes)
- 4 processes that must un on every master Node that control the master state and the workers
	- API Server(client interacts with this api)
		- acts as a load balances for the master nodes
		- require much less resources then worker nodes
		- more worker nodes, then more master nodes => makes the application more robust
		- cluster gateway
		- gets initial request like update or query
		- acts as a gatekeeper for authentication
		- validates a request and when OK it forwards the request to other processes and eventually to the Pod.
		- One entry point to the cluster
	- Scheduler
		- API Server sends request to Scheduler and will start a Pod
		- Has intelligence with Pod has to complete the work (how much cpu, ram etc)
		- Kubelet gets the request and executes the request
	- Controller manager
		- detects cluster state changes like crashes
		- tries to recover the state by making a request to the Scheduler
	- etcd (key value store off the cluster state) (cluster brain)
		- other 3 components get the information from etcd
		- does not contain any application data!
		- distributed across all the master nodes
	![4c5a7cb8f10e6e2cc67ccfb4681679b7.png](../../_resources/4c5a7cb8f10e6e2cc67ccfb4681679b7.png)
	
	