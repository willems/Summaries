import sys
import getopt

# programname = sys.argv[0]
# filename = sys.argv[1]
# message = sys.argv[2]

# print(programname)

# with open(filename, 'w+') as f:
#     f.write(message)

# Defaults
filename = "test.txt"
message = "Hello"

# no args, just kwargs
opts, args = getopt.getopt(sys.argv[1:], "f:m:", ["filename", "message"])

# call from commandline:
# python ArgumentParsing.py -f filename -m Hello\ World    Sequence of name arguments is inrelevant
# result:   [('-f', 'filename'), ('-m', 'Hello World')]

for opt, args in opts:
    if opt == "-f":
        filename = args
    if args == "-m":
        message = args

with open(filename, "w+") as f:
    f.write(message)
