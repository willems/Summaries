from abc import ABCMeta, abstractstaticmethod, abstractmethod


class IDepartment(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self) -> None:
        """interface implemented in child class"""

    @abstractstaticmethod
    def print_department():
        """interface implemented in child class"""


class Accounting(IDepartment):
    def __init__(self, employees) -> None:
        super().__init__()
        self.employees = employees

    def print_department(self):
        print(f"Accounting department: {self.employees}")


class Development(IDepartment):
    def __init__(self, employees) -> None:
        super().__init__()
        self.employees = employees

    def print_department(self):
        print(f"Development department: {self.employees}")


class ParentDepartment(IDepartment):
    def __init__(self, employees) -> None:
        super().__init__()
        self.employees = employees
        self.base_employees = employees
        self.sub_depts = []

    def add(self, dept):
        self.sub_depts.append(dept)
        self.employees += dept.employees

    def print_department(self):
        print("Parant Department")
        print(f"Parent Department Base Employees: {self.base_employees}")
        for dept in self.sub_depts:
            dept.print_department()
        print(f"Total number of employees: {self.employees}")


if __name__ == "__main__":
    dept1 = Accounting(200)
    dept2 = Development(150)

    parent_dept = ParentDepartment(30)
    parent_dept.add(dept1)
    parent_dept.add(dept2)

    parent_dept.print_department()
