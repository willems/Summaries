class Person:
    def __init__(self, name, age, gender):
        self.__name = name
        self.__age = age
        self.__gender = gender

    @property
    def Name(self):
        return self.__name

    @Name.setter
    def Name(self, value):
        self.__name = value

    @staticmethod
    def mymethod():
        return "Hello World!"


Person.mymethod()  # just a static method

p1 = Person("Bob", 20, "m")

print(p1.Name)

p1.Name = "Mike"
print(p1.Name)
