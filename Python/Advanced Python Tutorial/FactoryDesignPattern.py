from abc import ABCMeta, abstractstaticmethod


class IPerson(metaclass=ABCMeta):
    @abstractstaticmethod
    def person_method():
        """Interface method"""


class Student(IPerson):
    def __init__(self):
        self.name = "Basic student name"

    def person_method(self):
        print("I am a Student")


class Teacher(IPerson):
    def __init__(self):
        self.name = "Basic teacher name"

    def person_method(self):
        print("I am a teacher")


class PersonFactory:
    @staticmethod
    def build_person(person_type):
        if person_type == "Student":
            return Student()
        if person_type == "Teacher":
            return Teacher()
        print("Invalid Type")
        return -1


if __name__ == "__main__":
    choice = input("Do you want to create an Student or Teacher object?\n")
    person = PersonFactory.build_person(choice)
    person.person_method()
