from abc import ABCMeta, abstractstaticmethod


class IPerson(metaclass=ABCMeta):
    @abstractstaticmethod
    def person_method():
        """Interface Method"""


class Person(IPerson):
    def person_method(self):
        print("I am a person!")


class ProxyPerson(IPerson):
    def __init__(self) -> None:
        super().__init__()
        self.person = Person()

    def person_method(self):
        print(
            "I am the proxy functionality!"
        )  # when using the proxy pattern this functionality is used first
        self.person.person_method()


p1 = Person()
p1.person_method()

print("=" * 20)

p2 = ProxyPerson()
p2.person_method()
