from abc import ABCMeta, abstractstaticmethod

# for singlton multiple implementations are possible.


class IPerson(metaclass=ABCMeta):
    """Abstract Class"""

    @abstractstaticmethod
    def print_data():
        """implemented in child class"""


class PersonSingleton(IPerson):
    __instance = None  # place holder for single instance

    @staticmethod
    def get_instance():
        if PersonSingleton.__instance == None:
            PersonSingleton("Default Name", 0)
        return PersonSingleton.__instance

    def __init__(self, name, age) -> None:
        super().__init__()
        if PersonSingleton.__instance != None:
            raise Exception("Singleton cannot be instanciated more than once!")
        else:
            self.name = name
            self.age = age
            PersonSingleton.__instance = self

    @staticmethod
    def print_data():
        print(
            f"name: {PersonSingleton.__instance.name} and age: {PersonSingleton.__instance.age}"
        )


p = PersonSingleton("Mike", 20)
print(p)
p.print_data()

# p2 = PersonSingleton('Bob', 30)   # will raise an exception

p2 = PersonSingleton.get_instance()
print(p2)
p2.print_data()
