---
title: Algemeen
updated: 2021-05-04 14:58:11Z
created: 2021-05-04 14:58:11Z
---

# General Python

[Install Python](https://tecadmin.net/install-python-3-8-ubuntu/)

[Use Poetry: Python packaging and dependency management](https://python-poetry.org/)