---
title: Jupyter Built-in magic commands
updated: 2022-04-26 11:13:12Z
created: 2022-04-26 10:31:22Z
---

## references

%lsmagic # list of all magic methods
%quickref # cheatsheet
%magic
%lsmagic

## Timeit

%%timeit -n 3
%time

The %timeit magic runs the given code many times, then returns the speed of the fastest result.

```
%timeit sum(range(100000))
```

The %%timeit cell magic can be used to time blocks of code. Start cell

| Options | Description |
| --- | --- |
| -n&lt;N&gt; | It executes the code statement &lt;N&gt;times in a loop. If the number is not given, it determines the <n>to get good accuracy.</n> |
| -r&lt;R&gt; | Shows the number of repeats. |
| -p&lt;P&gt; | Used to calculate the precision of &lt;P&gt;digits to show the timing result. |
| -c  | Use time.clock; default function on Windows to measure the wall time. |
| -t  | Use time.time; the default function on Unix measures the wall time. |
| -q  | Use for Quiet; do not display any result. |
| -o  | Returns the TimeitResult that is further stored in a variable to view more details |

```
%%timeit -n 3
a = 0
for i in range(100000):
        a += i
```

The %time magic times a single run of a function

```
%time sum(range(100000))
```

## run scripts

```
%run somescript.py  
%run -d myscript.py # debug
```

## reset kernel

%reset is not a kernel restart

## Matplotlib

```
from matplotlib import pyplot as plt
%matplotlib inline
```

%matplotlib # set matplotlib to work interactively; does not import anythig
%matplotlib inline
%matplotlib qt # request a specific GUI backend

## debugging

%debug # jump into the Python debugger (pdb)
%pdb # start the debugger on any uncaught exception.

%cd # change directory
%pwd # print working directory
%env # OS environment variables

## OS command

!OScommand
!ping www.bbc.co.uk
%alias # system command alias
!!date # output \['Sat Jan 19 02:53:54 UTC 2019'\]
%system date

## Auto reload

%load_ext autoreload
%autoreload
When you are working with external tools or changing the enviornment variables, this will certainly help you a lot. The external commands help you autoreload the tools and libraries at a specified defined interval. So whenever there is even a minor change, we do not have to run the imports to update the local enviornment of the notebook