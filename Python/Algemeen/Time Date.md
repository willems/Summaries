---
title: Time Date
updated: 2022-04-25 10:57:17Z
created: 2022-04-25 10:53:38Z
---

```python
import datetime as dt
import time as tm
```

### seconds sinds 1 jan 1970
```python
tm.time()
```
Output
1650884042.68

```python
dt.datetime.fromtimestamp(tm.time())
```
Output:
datetime.datetime(2022, 4, 25, 12, 54, 44, 69803)

```python
dt.timedelta(days = 100) # create a timedelta of 100 days
```
