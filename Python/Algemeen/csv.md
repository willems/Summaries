```python
import csv

%precision 2

with open('../datasets/mpg.csv') as csvfile:
    mpg = list(csv.DictReader(csvfile))
```

output gives dict with key the row number:
[{'': '1',
  'manufacturer': 'audi',
  'model': 'a4',
  'displ': '1.8',
  'year': '1999',
  'cyl': '4',
  'trans': 'auto(l5)',
  'drv': 'f',
  'cty': '18',
  'hwy': '29',
  'fl': 'p',
  'class': 'compact'},
 {'': '2',
