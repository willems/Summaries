---
title: Installation
updated: 2022-07-11 20:02:16Z
created: 2021-05-04 14:58:11Z
---

# Conda

## Activate / Deactivate Conda

```bash
conda config --set auto_activate_base false
conda config --set auto_activate_base true
```

conda search [packagename]

conda install [packagename]
conda install --yes --file requirements.txt

conda update conda

### Environments

conda info --envs

conda remove --name  ENVNAME --all

conda create --name ENVNAME [optional packagename] optional [python=3.9]

conda env remove -n ENVNAME

conda activate ENVNAME

[conda-forge](https://conda-forge.org/)
conda install -c conda-forge jupyterlab

## Tensorflow

Just install tensorflow with GPU and all set.
No need anymore to install cuda, because this is installed together with TF python library

## Pip

### remove all pip lib

pip freeze --local > reqs.txt
pip uninstall -y -r reqs.txt

conda install -c conda-forge matplotlib ipykernel ipython pandas scikit-learn
