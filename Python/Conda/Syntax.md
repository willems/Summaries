---
title: Syntax
updated: 2022-06-19 10:43:57Z
created: 2021-11-01 17:08:37Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---

# Syntax

conda info

conda list \[-n ENV_NAME\]
conda list --revisions

conda search "^openCV$"
conda search "openCV"

conda install \[-c conda-forge\] <package-name>=x.y
conda install --revision 2</package-name>

conda update \[-n ENV_NAME\] --all
conda remove package-name

conda create -n ENV_NAME \[python=x.x\] ipython
conda create --name ENV\_NAME\_1 --clone ENV\_NAME\_2

conda env list
conda activate ENV_NAME
conda env remove -n ENV_NAME
conda remove -n ENV_NAME --all

`conda install -c conda-forge ipykernel pandas`