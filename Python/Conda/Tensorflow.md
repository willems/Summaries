---
title: Tensorflow
updated: 2022-06-29 11:34:19Z
created: 2022-06-03 16:38:35Z
---





conda create -n tf


conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0

pip install tensorflow



conda install mamba -n base -c conda-forge
mamba repoquery whoneeds ipython [-t]
[source Mamba](https://github.com/mamba-org/mamba)


conda install -c anaconda tensorflow-gpu
conda install tensorflow-gpu -c conda-forge







[source Tensorflow](https://www.tensorflow.org/install/pip)
