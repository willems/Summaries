---
title: Decorators
updated: 2022-04-03 12:04:11Z
created: 2021-05-04 14:58:11Z
---

# Decorators

[Decorators explained](https://lerner.co.il/2019/05/05/making-your-python-decorators-even-better-with-functool-wraps/)

```python
import attr

@attr.s(auto_attribs=True)
class Book(object):
    isbn: str
    name: str
    author: str
    published_year: int
    edition: int
```