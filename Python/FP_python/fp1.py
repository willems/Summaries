person = {"name": "Jan", "age": 34}

updated_person = {**person, "name": "Paul"}
print(updated_person)


# list and dictionaries
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def double(x):
    return x * 2


doubled_numbers = list(map(double, numbers))
print(doubled_numbers)


def is_even(x):
    return x % 2 == 0


even_numbers = list(filter(is_even, numbers))
print(even_numbers)

is_any_even = any(map(is_even, numbers))  # any element is the list is True???
print(is_any_even)
is_all_even = all(
    map(is_even, numbers)
)  # are all elements in the list even???
print(is_all_even)

# list comprehention

doubled_numbers2 = [double(x) for x in numbers]
doubled_numbers2 = [x * 2 for x in numbers]
print((doubled_numbers2))

is_all_even2 = [x for x in numbers if x % 2 == 0]
print(is_all_even2)

# The Unpacking operator
numbers_1 = [1, 2, 3]
numbers_2 = [4, 5, 6]

combine_list = [*numbers_1, *numbers_2]
print(combine_list)

person = {"name": "Jan", "age": 34, "hair_color": "blond"}
updated_person = {**person, "hear_color": "grey"}
print(updated_person)

combine_numbers = list(zip(numbers_1, numbers_2))
print(combine_numbers)

# The Unpacking operator with arguments
def print_args(*args):
    print(f'the args are:  {args}')

num = [1,2,3]
print_args(num) # pass a list as argument
print_args(*num) # pass every element of the list as an argument; result is a tuple

def print_kwargs(**kwargs):
    print(kwargs)
    
print_args(*person.values())
print_args(*person)

# First class funtions
