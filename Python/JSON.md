---
title: JSON
updated: 2022-04-03 12:15:44Z
created: 2022-01-17 17:27:47Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---

# JSON

```python
import json
from types import SimpleNamespace

# Initialize JSON data
json_data = '[{"studentid": 1, "name": "Nikhil", "subjects":\
["Python", "Data Structures"], "company":"GFG"}, \
{"studentid": 2, "name": "Nisha", "subjects":\
["Java", "C++", "R Lang"], "company":"GFG"}]'

x = json.loads(json_data, object_hook=lambda d: SimpleNamespace(**d))
print(x[0].subjects)
```


```python
class Employee(object):
def __init__(self,name1, name2):
self.name1 = name1
self.name2 = name2
def toJSON(self):
return json.dumps(self, default=lambda o: o.__dict__, 
sort_keys=True, indent=4)


ader = Employee('Olifant', 'aap')
json.loads(ader.toJSON())['name2']
```


```mermaid
graph TD;
A-->B;
A-->C;
B-->D;
C-->D;
```