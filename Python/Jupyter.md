---
title: Jupyter
updated: 2021-12-20 09:04:10Z
created: 2021-05-04 14:58:11Z
---

# Jupyter

```bash
pip install jupyterthemes
```

```bash
jt -l     # list available themes
jupyter-kernelspec list
jupyter --paths  # will list all of the possible locations for everything it uses to run: kernels, extensions, pidfiles, etc.
```

```bash
jupyter notebook --ip=0.0.0.0 --port=8080 --no-browser
jupyter lab --ip=0.0.0.0 --port=8080  --no-browser
```

```bash
export PYSPARK_DRIVER_PYTHON="/home/john/environments/notebook/bin/ipython"
```

```python
# Import findspark when not pip install pyspark
import findspark

# Initialize and provide path
findspark.init("/home/john/opt/spark")
# or let spark find it
findspark.init()
```

https://towardsdatascience.com/introducing-jupytext-9234fdff6c57
https://github.com/mwouts/jupytext

|Command|Description|
---|---|
|%quickref| Display the IPython Quick Reference Card|
|%magic| Display detailed documentation for all of the available magic commands|
|%debug| Enter the interactive debugger at the bottom of the last exception traceback|
|%hist| Print command input (and optionally output) history|
|%pdb| Automatically enter debugger after any exception|
|%paste| Execute preformatted Python code from clipboard|
|%cpaste| Open a special prompt for manually pasting Python code to be executed|
|%reset| Delete all variables/names defined in interactive namespace|
|%page| OBJECT Pretty-print the object and display it through a pager|
|%run script.py| Run a Python script inside IPython|
|%prun| statement Execute statement with cProfile and report the profiler output|
|%time| statement Report the execution time of a single statement|
|%timeit| statement Run a statement multiple times to compute an ensemble average execution time; useful for timing code with very short execution time|
|%who, %who_ls, %whos| Display variables defined in interactive namespace, with varying levels of information/verbosity|
|%xdel| variable Delete a variable and attempt to clear any references to the object in the IPython internals|
 %matplotlib inline|Matplotlib Integration|
|%load_ext lab_black| Code formatting during save | 
|%matplotlib widget| plot mathplot|
|%load_ext tensorboard| load tensorflow board|
|%matplotlib widget|Load Matplotlib |
 
