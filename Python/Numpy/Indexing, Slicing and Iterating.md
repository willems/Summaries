---
title: Indexing, Slicing and Iterating
updated: 2022-04-25 10:19:34Z
created: 2022-04-25 10:15:11Z
---

```python
a = np.array([[1,2], [3, 4], [5, 6]])
```
Output:
array([[1, 2],
       [3, 4],
       [5, 6]])

```python
# if we want to get multiple elements 
# for example, 1, 4, and 6 and put them into
# a one-dimensional array we can enter the
# indices directly into an array function
np.array([a[0, 0], a[1, 1], a[2, 1]])
```
Output:
array([1, 4, 6])

```python
# we can also do that by using another form of array indexing, which essentiall "zips" the first list and the
# second list up
print(a[[0, 1, 2], [0, 1, 1]])
```
Output:
array([1, 4, 6])


## Boolean Indexing
```python
print(a[a>5])   # like in Pandas
```
