---
title: Invert Image
updated: 2022-04-25 10:13:25Z
created: 2022-04-25 10:11:12Z
---

```python
from PIL import Image
from IPython.display import display

im = Image.open('image.tiff')
array=np.array(im)

mask=np.full(array.shape,255)

# Now let's subtract that from the modified array
modified_array=array-mask

# And lets convert all of the negative values to positive values
modified_array=modified_array*-1

# And as a last step, let's tell numpy to set the value of the datatype correctly
modified_array=modified_array.astype(np.uint8)

display(Image.fromarray(modified_array))
```
