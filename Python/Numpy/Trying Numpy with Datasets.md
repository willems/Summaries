---
title: Trying Numpy with Datasets
updated: 2022-04-25 10:28:56Z
created: 2022-04-25 10:20:03Z
---

```python
wines = np.genfromtxt("datasets/winequality-red.csv", delimiter=";", skip_header=1)
```

### So all rows combined but only the first column from them would be
```python
print("one integer 0 for slicing: ", wines[:, 0])
```

### But if we wanted the same values but wanted to preserve that they sit in their own rows we would write
```python
print("0 to 1 for slicing: \n", wines[:, 0:1])
```

### What if we want several non-consecutive columns? We can place the indices of the columns that we want into an array and pass the array as the second argument. Here's an example
```python
wines[:, [0,2,4]]
```


### We can specify data field names when using genfromtxt() to loads CSV data. Also, we can have numpy try and infer the type of a column by setting the dtype parameter to None
```python
graduate_admission = np.genfromtxt('datasets/Admission_Predict.csv', dtype=None, delimiter=',', skip_header=1,
                                   names=('Serial No','GRE Score', 'TOEFL Score', 'University Rating', 'SOP',
                                          'LOR','CGPA','Research', 'Chance of Admit'))
```

### We can retrieve a column from the array using the column's name for example, let's get the CGPA column and only the first five values.
```python
graduate_admission['CGPA'][0:5]
```

