---
title: Datetime
updated: 2022-05-03 19:17:05Z
created: 2022-05-03 19:02:29Z
---

# Converting column to datetime dtype while loading file.

## Create a date parser function
```python
d_parser = lambda x: pd.to_datetime(x) 
df = pd.read_csv(file_name.csv, parse_dates=['date_column'], date_parser=d_parser)
```

## If date is not in parseable format, use
```python
pd.to_datetime.strptime(x, format)
```
## Eg. format for '2017-03-13 04-PM' is '%Y-%M-%D %I-%p'
## Datetime Formatting Codes - http://bit.ly/python-dt-fmt


# convert the 'Date' column to datetime format
```python
df['Date_new']= pd.to_datetime(df['Date'])
```

```python
df['Dates'] = pd.to_datetime(df['Dates'], format='%y%m%d') 

df['Date'] = df['Date'].astype('datetime64[ns]')

dtype = pd.SparseDtype(np.dtype('datetime64[ns]'))
series = pd.Series(df.date, dtype=dtype)
df['date']=np.array(series)
```

[Source1](https://www.codegrepper.com/code-examples/python/convert+date+column+to+datetime+pandas)
[Source2](https://dataindependent.com/pandas/pandas-to-datetime-string-to-date-pd-to_datetime/)

```python
from datetime import date
df_min_gr[df_min_gr['Date'] > pd.Timestamp(2005, 2, 25)]
```