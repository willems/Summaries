---
title: GroupBy
updated: 2022-04-26 12:15:16Z
created: 2022-04-26 11:30:24Z
---

 ## three broad categories of data processing to happen during the apply step:
 - Aggregation of group data
```
df.groupby("cancellation_policy")['review_scores_value'].mean()
df.groupby("cancellation_policy").agg({"review_scores_value":(np.nanmean,np.nanstd),"reviews_per_month":np.nanmean})
```
 - Transformation of group data
 ```
 cols = ["cancellation_policy", "review_scores_value"]
 transform_df = df[cols].groupby("cancellation_policy").transform(np.nanmean)
 transform_df.rename(
    {"review_scores_value": "mean_review_scores"}, axis="columns", inplace=True
)
df = df.merge(transform_df, left_index=True, right_index=True)
 ```
 - Filtration of group data
 ```
 df.groupby("cancellation_policy").filter(
    lambda x: np.nanmean(x["review_scores_value"]) > 9.2
)
```

[source](https://ipython.readthedocs.io/en/7.23.0/interactive/magics.html)
[source2](https://coderzcolumn.com/tutorials/python/list-of-useful-magic-commands-in-jupyter-notebook-lab)