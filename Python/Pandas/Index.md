---
title: Index
updated: 2022-04-25 17:42:01Z
created: 2022-04-25 17:40:58Z
---

```python

df = df.set_index('time')
df = df.sort_index()


df = df.reset_index()
df = df.set_index(['time', 'user'])
```
