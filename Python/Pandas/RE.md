---
title: RE
updated: 2022-04-25 17:40:12Z
created: 2022-04-25 17:38:20Z
---

# So that looks pretty nice, other than the column names. But if we name the groups we get named columns out
```python
pattern="(?P<First>^[\w]*)(?:.* )(?P<Last>[\w]*$)"

# Now call extract
names=df["President"].str.extract(pattern).head()

# ===============
pattern="(^[\w]*)(?:.* )([\w]*$)"

# Now the extract function is built into the str attribute of the Series object, so we can call it
# using Series.str.extract(pattern)
dd = df["President"].str.extract(pattern)
dd.rename(columns={0:'first',1:'last'}).head(4)

# ==========================
df["Born"]=df["Born"].str.extract("([\w]{3} [\w]{1,2}, [\w]{4})")
df["Born"]=pd.to_datetime(df["Born"])


```
