---
title: Scales
updated: 2022-04-26 12:39:56Z
created: 2022-04-26 12:38:15Z
---

```
my_categories=pd.CategoricalDtype(categories=['D', 'D+', 'C-', 'C', 'C+', 'B-', 'B', 'B+', 'A-', 'A', 'A+'], ordered=True)
						   
# then we can just pass this to the astype() function
grades=df["Grades"].astype(my_categories)
```
Output:
excellent    A+
excellent     A
excellent    A-
good         B+
good          B
Name: Grades, dtype: category
Categories (11, object): [D < D+ < C- < C ... B+ < A- < A < A+]

Usage
```
grades[grades>"C"]
```