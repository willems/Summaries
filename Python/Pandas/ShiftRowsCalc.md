---
title: ShiftRowsCalc
updated: 2022-04-17 21:34:50Z
created: 2022-04-17 21:33:59Z
---

```python
import pandas as pd

df = pd.DataFrame(
    {
        "ts": [
            "2020-01-01 21:00",
            "2020-01-01 22:00",
            "2020-01-01 21:05",
            "2020-01-01 22:06",
            "2020-01-01 22:00",
            "2020-01-01 22:07",
            "2020-01-01 23:01",
            "2020-01-01 23:09",
            "2020-01-01 23:01",
            "2020-01-01 23:09",
        ],
        "action": [
            "start",
            "start",
            "stop",
            "stop",
            "start",
            "stop",
            "start",
            "stop",
            "start",
            "stop",
        ],
        "kol1": [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
        "name": [
            "aap",
            "noot",
            "aap",
            "noot",
            "aap",
            "aap",
            "noot",
            "noot",
            "olifant",
            "olifant",
        ],
        "kol2": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    }
)
df["day"] = pd.to_datetime(df.ts.str.slice(0, 10), format="%Y-%m-%d")
df["ts"] = pd.to_datetime(df["ts"], format="%Y-%m-%d %H:%M")

df1 = df[df["action"] == "start"]
df2 = df[df["action"] == "stop"]
df2.reset_index(inplace=True)
df1.reset_index(inplace=True)
df3 = df1.join(df2, lsuffix="_a")
df3["diff"] = (df3["ts"] - df3["ts_a"]) / pd.Timedelta(minutes=1)
df3.groupby(["day", "name"]).sum()
```