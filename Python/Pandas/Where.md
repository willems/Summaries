---
title: Where
updated: 2022-04-25 17:47:25Z
created: 2022-04-25 17:45:33Z
---

```python

admit_mask=df['chance of admit'] > 0.7
df.where(admit_mask)

# =======================

df[df["gre score"]>320]

# ========================

df['chance of admit'].gt(0.7) & df['chance of admit'].lt(0.9)
# or chained
df['chance of admit'].gt(0.7).lt(0.9)
```