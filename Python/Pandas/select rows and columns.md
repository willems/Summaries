---
title: select rows and columns
updated: 2022-04-25 12:11:53Z
created: 2022-04-25 12:06:56Z
---

df.loc['school1']   # school1 is an index name
df.T.loc['Name']   # first transpose and then a column name
df.loc[:,['Name', 'Score']]   # Name and Score are columns

df.drop('school1',axis=0)   # drop the rows with index name school1
df.drop("Name", inplace=True, axis=1)  # drop the column with name Name

