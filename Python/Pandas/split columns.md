---
title: split columns
updated: 2022-04-09 15:56:28Z
created: 2022-04-09 15:55:40Z
---

```python
import pandas as pd	
import re

d = {
    "kol1": ["iets", "nogiets", "nogietssss", "laatste"],
    "info": [
        "aap:1234,olifant:pp12,giraffe:34/0",
        "aap:6789,giraffe:36/0",
        "aap:0089,olifant:pp25,giraffe:40/0",
        "aap:0089,olifant:pp25,neushoorn:blabla",
    ],
    "test": ["a1", "b2", "c3", "d4"],
}

d1 = {
    "kol1": ["iets1", "nogiets1", "nogietssss1", "laatste1"],
    "info": [
        "{'aap':'1234','olifant':'pp12','giraffe':'34/0'}",
        "{'aap':'6789','giraffe':'36/0'}",
        "{'aap':'1189','olifant':'pp25','giraffe':'40/0'}",
        "{'aap':'0089','olifant':'pp25','neushoorn':'blabla'}",
    ],
    "test": ["a1", "b2", "c3", "d4"],
}

df = pd.DataFrame(d)
df1 = pd.DataFrame(d1)


def qq(x):
    return eval("{" + re.sub(r"([\w\/]+)", r'"\1"', x) + "}")


res = df.join(df["info"].apply(lambda x: pd.Series(qq(x)))).drop(["info"], axis=1)
res1 = df1.join(df1["info"].apply(lambda x: pd.Series(eval(x)))).drop(["info"], axis=1)

pd.concat([res, res1]).reset_index(drop=True)
```