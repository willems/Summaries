---
title: Installation & running
updated: 2021-11-08 11:13:17Z
created: 2021-11-01 13:05:19Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---


# Installation

01/11/2021 15:17

```python
import torch

torch.cuda.is_available()
torch.cuda.current_device()
torch.cuda.device_count()
torch.cuda.get_device_name(0)
```

```python
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('Using device:', device)
print()

#Additional Info when using cuda
if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')
```

```bash
watch -n 2 nvidia-smi
```

## run docker container

```bash
docker run -it --rm --gpus all --ipc=host --privileged -p 8888:8888 -v $(pwd):/workspace/fastbook fastdotai/fastai:latest ./run_jupyter.sh

```
