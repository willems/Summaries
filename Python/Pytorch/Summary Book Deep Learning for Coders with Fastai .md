---
title: Summary Book Deep Learning for Coders with Fastai and PyTorch
updated: 2021-11-08 14:15:52Z
created: 2021-11-08 11:13:26Z
---

## Jargon

- **architecture**: template of the model that we’re trying to fit; i.e., the actual mathematical function that we’re passing the input data and parameters to
- **parameters**: weights
- **independent variable**: data without labels
- **dependent variable**: label
- **label**: The data that we’re trying to predict, such as “dog” or “cat”
- **predictions**: result of the model
- **loss**: measure of performance. The entire purpose of **loss** is to define a “measure of performance” that the training system can use to update weights automatically
- **epochs**: an epoch is one complete pass through the dataset
- **hyperparameters**: parameters about parameters, since they are the higher-level choices that govern the meaning of the weight parameters.
- **CNN**: Convolutional Neural Network; a type of neural network that works particularly well for computer vision tasks
- **NLP**: natural language processing
- **segmentation** Creating a model that can recognize the content of every individual pixel in an image.

![31156c4015ea6f9c5ae11c6a02d28cbe.png](../../_resources/31156c4015ea6f9c5ae11c6a02d28cbe.png)

**Positive feedback look**: the more the model is used, the more biased the data
becomes, making the model even more biased, and so forth.

```python
from fastai.vision.all import *
path = untar_data(URLs.PETS)/'images'

def is_cat(x): return x[0].isupper()
dls = ImageDataLoaders.from_name_func(
 path, get_image_files(path), valid_pct=0.2, seed=42,
 label_func=is_cat, item_tfms=Resize(224))

learn = cnn_learner(dls, resnet34, metrics=error_rate, pretrained=True)
learn.fine_tune(epochs=1)
learn.show_results(max_n=6, figsize=(7,8))
```

- **Image**DataLoaders:  First part of name indicate type of data eq image or text
  - **Tabular**DataLoaders, **Text**DataLoaders or **Segmentation**DataLoaders
- **valid_pct**=0.2 size **validation set** (==developement set) used to **measure the accuracy of the model**. Remaining is the **training set**
- **seed**=42 random seed to thesame value every time we run this code, which means we **get the same validation set** every time we run it.

fastai will always show you your **model’s accuracy** using only the **validation set**
The longer you train (with not enough data) for, the better your accuracy will get on the training set; the validation set accuracy will also improve for a while, but eventually it will start getting worse as the model starts to **memorize the training set** rather than **finding generalizable underlying patterns in the data**. When this happens, we say that the model is **overfitting**.
**test set** hold back from the training process, we must hold back the test set data even from ourselves. It cannot be used to improve the model; it can be used only to evaluate the model at the very end of our efforts. Test data is totally hidden.
Hiring third party, then use a good test set for mitigating risks

![a8e058c2e06084485b3140b75a20e78e.png][def]
*Many methods to avoid overfitting, but use them only when confirmed that overfitting is occurring.*

- learn = **cnn_learner**(dls, resnet34, metrics=error_rate): convolutional neural network (CNN)
  - dls: the data
  - resnet34: 34 in resnet34 refers to the number of layers in the resnet
  Models using architectures with more layers take longer to train and are more prone to overfitting, when using more data, they can be quite a bit more accurate.
  - metric: measures the quality of the model’s predictions using the validation set, and will be printed at the end of each epoch.
  
![7e3dd277a7935550cabb842d0b3e91ef.png](../../_resources/7e3dd277a7935550cabb842d0b3e91ef.png)

- error_rate: what percentage of images in the validation set are being classified incorrectly
- accuracy = 1.0 - error_rate
- parameter **pretrained** defaults to True (*transfer learning*). When using a pretrained model, cnn_learner will remove the last layer and replace it with one or more new layers with randomized weights, of an appropriate size for the dataset you are working with. This last part of the model is known as the **head**.
- learn.**fine_tune**(epochs=1)
  - adapt a pretrained model for a new dataset
  - **fit** method will overwrite all weights of the pretrained model
- learn.**show_results**(max_n=6, figsize=(7,8))

Computer vision datasets are normally structured in such a way that the **label** for an image is part of the filename or path - most commonly the parent folder name.

A **Transform** contains code that is applied automatically during training:

- **item_tfms**: applied to each item eq item is resized to a 224-pixel square
- **batch_tfms**: applied to a batch of items at a time using the GPU

## upload a picture

```python
from fastbook import *  # requiered for widgets
uploader = widgets.FileUpload()
uploader
```

## test

```python
img = PILImage.create(uploader.data[0])
is_cat,_,probs = learn.predict(img)
print(f"Is this a cat?: {is_cat}.")
print(f"Probability it's a cat: {probs[1].item():.6f}")
```

**classification model**: attempts to predict a class, or category. That is, it’s predicting from a number of discrete possibilities, such as “dog” or “cat.”
**regression model** is one that attempts to predict one or more numeric quantities, such as a temperature or a location. Using y_range parameter to define the range of the targets

[def]: ../../_resources/a8e058c2e06084485b3140b75a20e78e.png
