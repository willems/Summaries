import unittest
from calculator import Calculator


class TestCalculator(unittest.TestCase):
    def setUp(self):
        self.calculator = Calculator()

    def test_add_numbers_returns_sum(self):
        result = self.calculator.add(20, 60)
        self.assertEqual(20 + 60, result)

        result = self.calculator.add(20.66, 60.56)
        self.assertEqual(20.66 + 60.56, result)

    def test_add_non_numbers_raises_type_error(self):
        self.assertRaises(TypeError, self.calculator.add, "hello", "world")
        self.assertRaises(TypeError, self.calculator.add, 59, "world")
        self.assertRaises(TypeError, self.calculator.add, "hello", 54)
        self.assertRaises(TypeError, self.calculator.add, "60.45.34.2", 29)


if __name__ == "__main__":
    unittest.main()
