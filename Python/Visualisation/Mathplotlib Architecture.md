---
title: Mathplotlib Architecture
updated: 2022-05-02 14:30:40Z
created: 2022-05-02 10:04:42Z
---


![0b8629bbcc1c0bf173fcefc7ea92df53.png](../../_resources/0b8629bbcc1c0bf173fcefc7ea92df53.png)


### difference between procedural and declarative visualization libraries
- pyplot scripting layer is a procedural method for building a visualization in that we tell the underlying software which drawing actions we want it to take in order to render our data. 
- declarative methods for visualizing data. HTML is a great example of this. The popular JavaScript library, for instance,D3.JS is an example of a declarative information visualization method. 

### Matlablib layers
- **backend**:
	concrete implementations of the abstract interface classes
		- FigureCanvas: a concrete implementation which knows how to insert itself into a native Qt window 
		- Renderer: provide a low-level drawing interface for putting ink onto the canvas.
		- Event: signals the callback dispatcher to generate the events so upstream listeners can handle them
```python
import numpy as np
import matplotlib.pyplot as plt

def on_press(event):
    if event.inaxes is None: return
    for line in event.inaxes.lines:
        if event.key=='t':
            visible = line.get_visible()
            line.set_visible(not visible)
    event.inaxes.figure.canvas.draw()

fig, ax = plt.subplots(1)

fig.canvas.mpl_connect('key_press_event', on_press)

ax.plot(np.random.rand(2, 20))

plt.show()
```
- **artist**: middle layer of the matplotlib stack, and is the place where much of the heavy lifting happens.
	Artist is the object that knows how to take the Renderer and put ink on the canvas. Everything you see in a matplotlib Figure is an Artist instance,
	The coupling between the Artist hierarchy and the backend happens in the draw method. The API works very well, especially for programmers, and is usually the appropriate programming paradigm when writing a web application server, a UI application, or perhaps a script to be shared with other developers.
	Two types of Artist:
	- **Primitive artists** represent the kinds of objects you see in a plot: Line2D, Rectangle, Circle, and Text.
	- **Composite artists** are collections of Artists such as the Axis, Tick, Axes, and Figure. Each composite artist may contain other composite artists as well as primitive artists. The most important composite artist is the Axes.
- **scripting**.
	-  Most special-purpose languages for data analysis and visualization provide a lighter scripting interface to simplify common tasks, and matplotlib does so as well in its matplotlib.pyplot interface.
	-  pyplot is a stateful interface that handles much of the boilerplate for creating figures and axes and connecting them to the backend of your choice, and maintains module-level internal data structures representing the current figure and axes to which to direct plotting commands.
	```
	import matplotlib.pyplot as plt
	```
	- When the pyplot module is loaded, it parses a local configuration file in which the user states, among many other things, their preference for a default backend.
	- The pyplot interface is a fairly thin wrapper around the core Artist API which tries to avoid as much code duplication as possible by exposing the API function, call signature and docstring in the scripting interface with a minimal amount of boilerplate code.

### matplotlib spends a lot of time transforming coordinates from one system to another. These coordinate systems include:
- data: the original raw data values
- axes: the space defined by a particular axes rectangle
- figure: the space containing the entire figure
- display: the physical coordinates used in the output (e.g. points in PostScript, pixels in PNG)

[source](https://www.aosabook.org/en/matplotlib.html)

