---
title: Matplotlib Code
updated: 2022-05-02 14:33:55Z
created: 2022-05-02 14:30:43Z
---

- pyplot is going to retrieve the current figure with the function **gcf** and then get the current axis with the function **gca**. 
- pyplot just mirrors the API of the axis objects. So you can call the plot function against the pyplot module. But this is calling the axis plot functions underneath, so be aware.
- the function declaration from most of the functions in matplotlib end with an open set of keyword arguments. 


