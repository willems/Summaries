---
title: Ten Simple Rules for Better Figures
updated: 2022-05-02 12:41:23Z
created: 2022-05-02 12:05:33Z
---

## Rule 1: Know Your Audience
 it is important to identify, as early as possible in the design process, the audience and the message the visual is to convey. The graphical design of the visual should be informed by this intent.

## Rule 2: Identify Your Message
A figure is meant to express an idea or introduce some facts or a result that would be too long (or nearly impossible) to explain only with words, be it for an article or during a time-limited oral presentation. In this context, it is important to clearly identify the role of the figure, i.e., what is the underlying message and how can a figure best express this message? Once clearly identified, this message will be a strong guide for the design of the figure.

## Rule 3: Adapt the Figure to the Support Medium
A figure can be displayed on a variety of media, such as a poster, a computer monitor, a projection screen (as in an oral presentation), or a simple sheet of paper (as in a printed article). Each of these media represents different physical sizes for the figure, but more importantly, each of them also implies different ways of viewing and interacting with the figure. 

## Rule 4: Captions Are Not Optional
you cannot explain everything within the figure itself—a figure should be accompanied by a caption. The caption explains how to read the figure and provides additional precision for what cannot be graphically represented. 

## Rule 5: Do Not Trust the Defaults
Since settings are to be used for virtually any type of plot, they are not fine-tuned for a specific type of plot. In other words, they are good enough for any plot but they are best for none. All plots require at least some manual tuning of the different settings to better express the message, be it for making a precise plot more salient to a broad audience, or to choose the best color-map for the nature of the data.

## Rule 6: Use Color Effectively
If you have no need for color, you need to ask yourself, “Is there any reason this plot is blue and not black?” If you don't know the answer, just keep it black. The same holds true for color-maps. Do not use the default color-map (e.g., jet or rainbow) unless there is an explicit reason to do so.
- Sequential: one variation of a unique color, used for quantitative data varying from low to high.
- Diverging: variation from one color to another, used to highlight deviation from a median value.
- Qualitative: rapid variation of colors, used mainly for discrete or categorical data.

## Rule 7: Do Not Mislead the Reader
Using pie charts or 3-D charts to compare quantities. These two kinds of plots are known to induce an incorrect perception of quantities and it requires some expertise to use them properly. As a rule of thumb, make sure to always use the simplest type of plots that can convey your message and make sure to use labels, ticks, title, and the full range of values when relevant. 

## Rule 8: Avoid “Chartjunk”
Unnecessary or confusing visual elements found in a figure that do not improve the message (in the best case) or add confusion (in the worst case). (Edward Tutfe)
[Junk Charts](https://junkcharts.typepad.com/)

## Rule 9: Message Trumps Beauty
most of the time, you may need to design a brand-new figure, because there is no standard way of describing your research. In such a case, browsing the scientific literature is a good starting point. If some article displays a stunning figure to introduce results similar to yours, you might want to try to adapt the figure for your own needs

## Rule 10: Get the Right Tool

[Source](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003833)