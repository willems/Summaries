---
title: Theory
updated: 2022-04-26 16:48:19Z
created: 2022-04-26 14:38:52Z
---

Alberto Cairo (Visualization Wheel)
[Alberto Cairo's Visualization Wheel](https://ryanwingate.com/visualization/guidelines/visualization-wheel/)

### What's a heuristic?
A heuristic is a process or rule that is meant to guide you in decision making. Is by definition not known to be optimal or perfect, but to be practical in nature. Heuristics are meant to be followed until you've a reason to deviate from them.

Edward Tufte (book: The Visual Display of Quantitative Information)
introduces two interesting **graphical heuristics**:
- the data-ink ratio
Tufte's first graphical heuristic is data-ink ratio.
Tufte defines data-ink as the non-erasable core of a graphic. In other words, the data-ink is essential to the	sense-making process for a given variable. 
Example
![95570cc80acedfbfa19a48f3f89b9024.png](../../_resources/95570cc80acedfbfa19a48f3f89b9024.png)
[More examples](https://www.darkhorseanalytics.com/)

- chart junk
Tufte is much more damning of chart-junk than he is of other forms of non-data ink.
He suggests that artistic decorations on statistical graphs are like weeds in our data graphics. 
There's really three kinds of chart-junk:
	-	**unintended optical art**. For instance, excessive shading or patterning of chart features
	![8bc6b50bc979e6571d513235dd90457d.png](../../_resources/8bc6b50bc979e6571d513235dd90457d.png)
	These phenomenon are called moiré patterns.
	- Tufte suggested the **grid is both unnecessary as data ink**, but also causes competition with the actual data being shared. Thinning, removing, or de-saturating grid lines makes it easier to see the data, instead of being overwhelmed by the number of lines on the page. **Direct labeling of data** is another great way to reduce this form of chart-junk. 
	- the duck. Broadly, Tufte is referring to non-data creative graphics, whether they be line art or photographs and they're inclusion in the chart.
As you go about creating your data science graphics, it's worth not only reflecting on the principles you use and the results you are sharing, but also the process by which you came up to create the graphics.