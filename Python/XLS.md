---
title: XLS
updated: 2022-01-17 20:43:48Z
created: 2022-01-17 20:43:09Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---

```bash
pip install openpyxl
```

```python
employment = pd.read_excel("data/unemployment.xlsx")
```