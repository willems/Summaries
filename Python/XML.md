```url
https://www.programcreek.com/python/example/88288/xml.etree.ElementTree.ParseError
```
```python
try:
    from xml.etree.ElementTree import ParseError
except ImportError as e:
    from xml.parsers.expat import ExpatError as ParseError


try:
            xml_root = ET.fromstring(response._content)
            for xml_child in xml_root.findall('SD//'):
                if xml_child.tag in alexa_keys and \
                        alexa_keys[xml_child.tag] in xml_child.attrib:
                    attributes[xml_child.tag.lower(
                    )] = xml_child.attrib[alexa_keys[xml_child.tag]]
        except ParseError:
            # Skip ill-formatted XML and return no Alexa attributes
            pass
```


