# Collections

## Rust’s collections can be grouped into four major categories:

- **Sequences**: Vec, VecDeque, LinkedList
- **Maps**: HashMap, BTreeMap
- **Sets**: HashSet, BTreeSet
- **Misc**: BinaryHeap

Most used **Vec** and **HashMap**
[Vec examples and usage](https://doc.rust-lang.org/std/vec/struct.Vec.html)

[HashMap examples and usage](https://doc.rust-lang.org/std/collections/hash_map/struct.HashMap.html)

## When use Vec

- collect items up to be processed or sent elsewhere later, and don’t care about any properties of the actual values being stored.
- a sequence of elements in a particular order, and will only be appending to (or near) the end.
- want a <u>stack</u>.
- want a <u>resizable</u> array.
- want a <u>heap-allocated</u> array.

## When use a HashMap

- associate arbitrary keys with an arbitrary value (<u>key value pair</u>)
- cache
- map, with no extra functionality

## All other collection types

[Module collections](https://doc.rust-lang.org/std/collections/index.html)
