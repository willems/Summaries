Here is a summary of the Rust collections Vector, HashMap, and HashSet with essential examples GPT-4o mini):

**Vector**:
A Vector is a dynamic array that can grow or shrink in size. It stores elements of the same data type. Some key points about Vectors:

- Created using `vec![]` macro or `Vec::new()` constructor.
- Elements can be accessed using indexing, e.g., `my_vec[index]`.
- Methods like `push()`, `pop()`, `insert()`, `remove()` can be used to modify the Vector.
- Iterates over elements using a for loop, e.g., `for item in my_vec { ... }`.

Example:

```rust
let mut numbers: Vec<i32> = vec![1, 2, 3];
numbers.push(4);
println!("{:?}", numbers); // Output: [1, 2, 3, 4]
```

**HashMap**:
A HashMap is an unordered collection of key-value pairs. It allows efficient lookup, insertion, and deletion of elements.

- Created using `HashMap::new()` or `hashmap!{}` macro from the `hashmap` crate.
- Elements are accessed using the key, e.g., `my_map[&key]`.
- Methods like `insert()`, `get()`, `remove()` can be used to modify the HashMap.
- Iterates over key-value pairs using a for loop, e.g., `for (key, value) in my_map.iter() { ... }`.

Example:

```rust
use std::collections::HashMap;

let mut scores: HashMap<String, i32> = HashMap::new();
scores.insert("Blue".to_string(), 10);
scores.insert("Yellow".to_string(), 50);
println!("{:?}", scores); // Output: {"Blue": 10, "Yellow": 50}
```

**HashSet**:
A HashSet is an unordered collection of unique elements. It is useful for quickly checking if an element is present in the set.

- Created using `HashSet::new()` or `hashset!{}` macro from the `hashset` crate.
- Elements are added using the `insert()` method and checked for membership using the `contains()` method.
- Iterates over elements using a for loop, e.g., `for item in my_set { ... }`.

Example:

```rust
use std::collections::HashSet;

let mut fruits: HashSet<&str> = HashSet::new();
fruits.insert("apple");
fruits.insert("banana");
fruits.insert("cherry");
println!("{:?}", fruits); // Output: {"apple", "banana", "cherry"}
```

These are the essential features and examples of the Rust collections Vector, HashMap, and HashSet. They provide powerful data structures for various use cases in Rust programming.
