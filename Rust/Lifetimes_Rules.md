# Rust Lifetimes Rules

Based on the provided search results, here are the Rust lifetime rules:

1. **Lifetime Elision Rules**: Rust has three rules for eliding lifetimes:
    - **First Rule**: Each parameter gets its own lifetime. For example,

     ```rust
     fn foo<'a>(x: &'a i32)
    ```

    has a distinct lifetime 'a for the x parameter.
    - **Second Rule**: If there is exactly one input lifetime, that lifetime is assigned to all output lifetimes. For example,

    ```rust
    fn foo<'a>(x: &'a i32) -> &'a i32
    ```

    assigns the input lifetime 'a to the output lifetime.
    - **Third Rule**: If there are multiple input lifetimes and one of them is &self or &mut self (due to a method), the lifetime of self is assigned to all output lifetimes. This rule applies only to methods, not functions.

2. **Implicit Lifetimes**: Rust provides implicit lifetimes for certain cases:
    - **Static Lifetime**: 'static is a special lifetime that signifies a reference is valid for the entire program. It’s often used with string literals.
    - **Elided Lifetimes**: Rust can elide lifetimes in certain situations, such as when a reference is not used outside its immediate scope or when the lifetime can be inferred from the context.

3. **Borrow Checker**: Rust’s borrow checker enforces the lifetime rules by checking the validity of references at compile-time. It ensures that references do not outlive their valid scopes and that multiple references to the same data do not overlap.

4. **Lifetime Annotations**: While Rust can often infer lifetimes automatically, explicit lifetime annotations are required in certain situations, such as:
    - **Generic Lifetimes**: When defining generic types or traits, lifetimes must be explicitly annotated.
    - **Trait Objects**: When working with trait objects, lifetimes must be explicitly annotated to ensure correct borrowing and lifetime relationships.

5. **Coarse Lifetime Semantics**: Rust’s lifetime system is designed to be coarse-grained, meaning it focuses on the overall structure of the program rather than the fine-grained details of individual references. This allows for more efficient and scalable borrowing and lifetime checking.

## Resources

1. Rust by example: [Lifetime](https://doc.rust-lang.org/rust-by-example/scope/lifetime.html)
2. Rust by example: [Elision](https://doc.rust-lang.org/rust-by-example/scope/lifetime/elision.html)
