# References and Borrowing

In Rust, references and borrowing are __mechanisms that allow you to access and manipulate the contents of owned values without transferring ownership.__ This concept is fundamental to Rust’s memory safety and ownership system.

## Types of References

1. __Immutable Reference (&T)__: A reference that cannot be modified. It’s created using the unary & operator.
2. __Mutable Reference (&mut T)__: A reference that can be modified. It’s created using the unary &mut operator.

## Rules for Borrowing

1. __Exclusive Access__: A mutable reference (&mut T) has exclusive access to the underlying value, preventing any other references (mutable or immutable) from accessing it.
2. __Non-Overlapping Scopes__: References must have non-overlapping scopes. A reference’s scope is the region of code where it is valid.
3. __Lifetime__: Each reference has a lifetime, which represents the scope for which the borrow is valid.

## Coercions

1. __Immutable Reference Coercion__: A mutable reference (&mut T) can be coerced into an immutable reference (&T) with the same referent type.
2. __Lifetime Coercion__: References with longer lifetimes can be coerced into references with shorter lifetimes.

## Equality

1. __Reference Equality__: Two references are equal if they point to the same location in memory.
2. __Value Equality__: Two references are equal if they point to values with the same contents (using PartialEq).

## Traits Implemented for References

1. __All traits except Copy and Clone__ for &mut T references (to prevent creating multiple simultaneous mutable borrows).
2. __Additional traits__ for &T references if the underlying type T implements those traits.

## Send and Sync

1. __Send__: A reference is Send if the underlying type T is Send.
2. __Sync__: A reference is Sync if the underlying type T is Sync.

## Best Practices

1. __Use immutable references whenever possible__ to avoid unnecessary mutations.
2. __Use short-lived references__ to minimize the scope of borrows and reduce the risk of data races.
    Avoid creating multiple simultaneous mutable borrows to prevent data corruption.

## Common Errors

1. __Borrow Checker Errors__: Rust’s borrow checker will prevent you from creating invalid borrows, such as trying to create a mutable reference to a value that’s already borrowed immutably.
2. __Data Races__: Failing to follow the borrowing rules can lead to data races, where multiple threads access and modify shared data concurrently.

## Resources

1. The Rust Book: [References and Borrowing](https://doc.rust-lang.org/book/ch04-02-references-and-borrowing.html)
2. Rust By Example: [Borrowing](https://doc.rust-lang.org/rust-by-example/scope/borrow.html)
