---
title: Definitions
updated: 2022-05-21 13:16:46Z
created: 2022-05-21 12:20:35Z
---

Agnostic (data):
- In computing, a device or software program is said to be agnostic or data agnostic if the method or format of data transmission is irrelevant to the device or program’s function. This means that the device or program can receive data in multiple formats or from multiple sources, and still process that data effectively. [source](https://en.wikipedia.org/wiki/Agnostic_(data))
- Back-pressure: if downstream is not keeping up with the flow of data the way slowdown the data-flow => take care sources produce less data or pause for a little while.
- control latency vs throughput: process a specific message as fast as possible and get it to it's destination. On the other side process all data as fast as possible, so throughput is as much as possible so sacrifice a few seconds latency
