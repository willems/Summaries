---
title: IAM
updated: 2022-01-09 15:17:28Z
created: 2022-01-09 13:20:59Z
latitude: 52.38660000
longitude: 5.27820000
altitude: 0.0000
---

# Identity & Access Management (IAM)

Everything that helps in managing identities and their access. It is about the “who” and the “what”: Who can do what in IT? Who can log on? Who can access which data?

## Identity Management (abbr. of IAM)

Include partners (B2P), customers and consumers (B2C), as well as non-human entities that have identities such as services (software processes) and internet-connected devices that make up the Internet of Things (IoT)
The challenge is for organizations:

- manage all these identities to meet security and privacy requirements
- enabling business growth
- frictionless consumer/customer interaction
- personalized or tailored services and content.
    Therefor appropriate strategy and loosely coupled, extensible and service-orientated IT architecture in place to enable a smooth transition to the **as-a-service model**, both in terms of service consumption (to reduce costs and boost productivity) and service provision (to add new revenue streams and improve consumer/customer engagement).

*Essentially, the success of Digital Transformation depends on an ability to manage the access of everyone and everything to every digital service*.

Organizations should plan to support **all kinds of identities** and ensure they have the tools to understand the level of assurance provided by each identity type so they can make informed decisions on how those identities can be used for specific transactions or interactions using risk-based scoring, and adaptive authentication and authorization systems.
Agile and flexible by separating identity and applications, and providing the backend systems required to make all the necessary connections using Application Program Interfaces (APIs) that bridge services, microservices and containers
These changes will result in a converged digital identity backend or **“Identity Fabric”** that can deliver as a utility all the identity services (including security and privacy) required by the growing number of new digital services enabled by digital transformation that will actively consume identity services.

![bfd9c881c6664a63e2334228e4e57f83.png](../../_resources/bfd9c881c6664a63e2334228e4e57f83.png)
Also directory services as sort of a database of all the users, and Identity Verification for the first-time proof of an identity during the on-boarding process, e.g.,

## IAM consists of three core technologies (Traditional):

- **User Lifecycle Management & Access Governance** ( ==\> **IGA (Identity Governance & Administration)**)
    - **User Lifecycle Management** is also called **Identity Provisioning**.
    - The entire identity management process from creating a digital identity when on-boarding a person such as a new employee, to retiring that digital identity. **Join, Move, Leave (JML)**
    - The other part of User Lifecycle Management & Access Governance is about **requesting, approving, and reviewing** access entitlements. (who should have access to which systems, applications, and data)
    - **Access reviews** are required for regulatory compliance, to check regularly whether the access is still needed or must be revoked.
    - User Lifecycle Management is tightly coupled with **Identity Provisioning**: technically connecting IGA with the target systems such as Microsoft Active Directory or SAP, for e.g., creating user accounts, and assigning entitlements.
    - <img src="../../_resources/19949739424c8962762275254b28a831.png" alt="19949739424c8962762275254b28a831.png" width="761" height="700" class="jop-noMdConv">
- Access Management & Federation
    - Access to systems at runtime. Requires authentication, i.e. the verification that credentials such as user name and password have been entered correctly. Federation to other systems, a concept where for instance a user is authenticated in his organization and then logs on to the organization’s tenant of a SaaS (Software as a Service) application such as Salesforce.
    - **Access federation** is based on established standards such as OAuth2, SAML (Security Assertion Markup Language), and OIDC (OpenID Connect). The norm is SCIM.
    - ![90294faf40d4ca7b98565abeec1db421.png](../../_resources/90294faf40d4ca7b98565abeec1db421.png)
- **Privileged Access Management PAM** ==> Administrators
    - most technical discipline of IAM that deals with the specifics of highly privileged users such as administrators logging into systems, e.g., as Windows administrator or root user on Linux.
    - Some of the aspects covered by PAM solutions are rotating passwords for shared accounts, and session management.
    - Shared accounts such as root can be used by multiple persons.
    - To mitigate the risks, users of privileged accounts must not use the same password.
    - PAM ensures (amongst many other features) that privileged account users always get a new password for each new access.
    - The session management part of PAM is about recording sessions, e.g., for forensics, and for logging and monitoring what happens during privileged access.

## Con of Traditional IAM

**Creating user accounts** in a range of systems such as Microsoft Active Directory, Microsoft Azure Active Directory, LDAP servers, on email servers, mainframes, business applications such as SAP, specialized banking apps, new SaaS services, and many more ***requires technical integration*** with these systems via connectors. That part of integration is **challenging**, despite **SCIM** (System for Cross-domain Identity Management) becoming increasingly established as a standard.

## New trend towards IDaas (Identity as a Service)
- deployed using as-a-service models, avoiding the complex installation and operation of IAM on-premises.
-  integration with legacy backends such as mainframes still requires setting up connectors.
- 