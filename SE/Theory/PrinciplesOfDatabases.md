---
title: PrinciplesOfDatabases
updated: 2022-04-27 17:44:58Z
created: 2021-05-04 14:58:11Z
---

A **database** can be defined as a collection of related data items within a specific business process or problem setting

A **database management system** (DBMS) is the software package used to define, create, use, and maintain a database. It typically consists of several software modules, each with their own functionality

The combination of a DBMS and a database is then often called a **database system**

The **database model or database schema** provides the description of the database data at different levels of detail and specifies the various data items, their characteristics, and relationships, constraints, storage details, etc. The database model is specified during database design and is not expected to change frequently. It is stored in the catalog

The **database state** then represents the data in the database at a particular moment


[CAP Theorem](https://www.ibm.com/cloud/learn/cap-theorem)

[Docker: how to run PostgreSQL and pgAdmin using Docker?](https://medium.com/quick-code/how-to-run-postgresql-and-pgadmin-using-docker-90638fde8bf)