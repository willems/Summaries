# Time series

A time series is a set of data points ordered in time.
The data is equally spaced in time, meaning that it was recorded at every hour, minute, month, or quarter.  Time series are indexed by time, and that order must be kept.

Decomposition is defined as a statistical task that separates a time series into its different components:

![Trend](images/Trend.png)
The **trend** is defined
as the slow-moving changes in a time series.
If you observe seasonality, then a **SARIMA model** would be relevant, because this model uses seasonal effects to produce forecasts

![Seasonality](images/Seasonallity.png)
 The **seasonal** component captures the seasonal variation, which is a cycle that occurs over a fixed period of time.

**Residuals**, which is what cannot be explained by either the trend or the seasonal components. Residuals usually correspond to random errors, also termed **white noise**

![Seasonal and Cyclical](images/Seasonal_and_cyclical.png)

![Seasonal and Trend](./images/Seasonallity_trend.png)

![Variation](images/Variation.png)

![Bird’s-eye view of time series forecasting](images/ForcastingProjectRoadmap.png)

> The only way to know that a model is good, or performant, is to compare it to a baseline. The baseline model is the simplest solution you can
think of—it should not require any training, and the cost of implementation should be very low.
A **baseline model** is a trivial solution to your forecasting problem. It relies on heuristics or simple statistics and is usually the simplest solution. It does not require
model fitting, and it is easy to implement.

For example:

- arithmetic mean
- if we see a cyclical pattern in our data, we can simply repeat that pattern into the future.


Error-Trend-Seasonality (ETS)
Autoregressive Integrated Moving Average (ARIMA)
Explore advanced forecasting techniques with SARIMA and SARIMAX models


[Forecasting at Uber: An Introduction](https://www.uber.com/en-IN/blog/forecasting-introduction/)

- **strategic forecasts**, such as those predicting revenue, production, and spending
- **short-term tactical forecasts**, such as the amount of goods to be ordered and number of employees needed.

A person uses **spatial-temporal** reasoning to solve multi-step problems by envisioning how objects move in space and time.
[Explaned](https://www.heavy.ai/technical-glossary/spatial-temporal)

**quantitative forecasting approaches** can be grouped as follows:

- model-based or causal classical: strongest choice when the underlying mechanism, or physics, of the problem is known
- statistical methods (ARIMA, Holt-Winters, Theta method): underlying mechanisms are not known or are too complicated, e.g., the stock market and retail.
- machine learning approaches: black-box type and are used when interpretability is not a requirement.

| Classical & Statistical                           | Machine Learning                 |
| ------------------------------------------------- | -------------------------------- |
| Autoregressive integrated moving average (ARIMA)  | Recurrent neural networks (RNN)  |
| Exponential smoothing methods (e.g. Holt-Winters) | Quantile regression forest (QRF) |
| Theta                                             | Gradient boosting trees (GBM)}   |
|                                                   | Support vector regression (SVR)  |
|                                                   | Gaussian Process regression (GP) |

**Exogenous variables** (e.g., weather, concerts, etc.)
It is important to carry out **chronological testing** since time series ordering matters.
Train on a set of data that is older than the test data.
![Two major approaches to test forecasting models](https://blog.uber-cdn.com/cdn-cgi/image/width=2160,quality=80,onerror=redirect,format=auto/wp-content/uploads/2018/09/image6-e1536165830511.png)

- In the **sliding window approach**, one uses a fixed size window, shown here in black, for training. Subsequently, the method is tested against the data shown in orange.
- the **expanding window approach** uses more and more training data, while keeping the testing window size fixed. The latter approach is particularly useful if there is a limited amount of data to work with.
- often best, to marry the two methods

evaluation metrics:

- absolute errors and percentage errors
- One particularly useful approach is to compare model performance against the naive forecast:
    * non-seasonal series, a naive forecast is when the last value is assumed to be equal to the next value.
    * For a periodic time series, the forecast estimate is equal to the previous seasonal value

**Determining the best forecasting method** for a given use case is only one half of the equation. We also need to **estimate prediction intervals**. The prediction intervals are upper and lower forecast values that the actual value is expected to fall between with some (usually high) probability, e.g. 0.9
![Prediction intervals are critical to informed decision making.](https://blog.uber-cdn.com/cdn-cgi/image/width=2160,quality=80,onerror=redirect,format=auto/wp-content/uploads/2018/09/image2.png)

Prediction intervals are just as important as the point forecast itself and should always be included in your forecasts. Prediction intervals are typically a function of how much data we have, how much variation is in this data, how far out we are forecasting, and which forecasting approach is used.


## Deep Learning for Time Series Forecasting

### Exploring the Neural Network Landscape
- Long Short-Term Memory Networks (LSTMs):
    LSTMs are a type of Recurrent Neural Network (RNN) specifically designed to avoid the long-term dependency problem. They are capable of learning and remembering information over extended time intervals. This makes them particularly suitable for time series data, where understanding the context and dependencies across time is crucial.
    LSTMs have been used to predict stock prices.
- Gated Recurrent Units (GRUs):
    GRUs are a variation of LSTMs, known for their efficiency and simplicity. They combine the forget and input gates into a single “update gate,” making them faster to train than LSTMs while still effectively capturing temporal dependencies. GRUs are ideal for situations where computational efficiency is a priority, and have found use in applications like weather forecasting and speech recognition.
- Convolutional Neural Networks (CNNs):
    While predominantly known for image processing, CNNs have also been adapted for time series forecasting. They excel at identifying spatial hierarchies in data, making them effective in scenarios where the input data can be treated as a series of local patterns. This attribute makes CNNs useful for analyzing timeseries with complex, layered structures, such as electroencephalogram (EEG) signal analysis or predicting seismic activities.

### Challenges and Best Practices in Implementing Deep Learning for Time Series Data
- Data Preprocessing:
    Time series data often requires extensive preprocessing, including dealing with missing values, noise reduction, and normalization.
- Model Complexity:
    Choosing the right model architecture and tuning its parameters can be daunting.
- Overfitting:
    Deep learning models are prone to overfitting, especially when dealing with large datasets. Regularization techniques, such as dropout and early stopping, can help mitigate this issue.

### Best Practices
1. Start with Simple Models:
2. Hyperparameter Tuning
3. Cross-Validation
4. Continuous Monitoring


## PatchTSMixer
    is a lightweight time-series modeling approach based on the _multi-layer perceptron_ (MLP) MLP-Mixer architecture. Supports various attention mechanisms starting from simple gated attention to more complex self-attention blocks that can be customized accordingly. The model can be pretrained and subsequently used for various downstream tasks such as forecasting, classification and regression.




