---
title: Engels
updated: 2022-05-24 19:40:49Z
created: 2022-05-21 12:14:13Z
---

| Engels      | Nederlands                             |
| ----------- | -------------------------------------- |
| pursuing    | achtervolgen                           |
| unification | eenwording                             |
| provenance  | herkomst                               |
| versatile   | veelzijdig,veranderlijk, onstandvastig |


